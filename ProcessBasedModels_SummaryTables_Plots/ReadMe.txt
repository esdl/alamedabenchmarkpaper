Introduction:
------------

This ReadMe file describes the contents of the ProcessBasedModels_SummmaryTables_Plots folder.  
The structure of that folder is described below, along with instructions 
on how to use the included scripts to reproduce the analysis in the paper.



Instructions for reproducing the analysis in the paper:
-------------------------------------------------------
1) Run the uncalibrated and calibrated IHACRES models for both watersheds using: 
   \ProcessBasedModels_SummmaryTables_Plots\src\01_ihacres.R

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\results\01_ihacres


2) Run the uncalibrated and calibrated SAC-SMA models for both watersheds using: 
   \ProcessBasedModels_SummmaryTables_Plots\src\02_sacsma.R

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\results\02_sacsma


3) Run the uncalibrated PRMS model for both watersheds by double-clicking the Windows batch file:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_uncal_default\gsflow_onlyPRMS\gsflow\prms_run_AH_UA.bat

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_uncal_default\gsflow_onlyPRMS\gsflow\output


4) To re-calibrate the PRMS model for the Arroyo Hondo watershed, instructions can be found at:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ah\gsflow_onlyPRMS_03_20220415\instructions_for_running_prms_sce_calibration.pptx

   Run the already-calibrated PRMS model for the Arroyo Hondo watershed by double-clicking the Windows batch file:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ah\gsflow_onlyPRMS_03_20220415\gsflow\prms_run_ah_only.bat

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ah\gsflow_onlyPRMS_03_20220415\gsflow\output


5) To re-calibrate the PRMS model for the Upper Alameda watershed can be found at:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ua\gsflow_onlyPRMS_02_20220415\instructions_for_running_prms_sce_calibration.pptx

   Run the already-calibrated PRMS model for the Upper Alameda watershed by double-clicking the Windows batch file:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ua\gsflow_onlyPRMS_02_20220415\gsflow\prms_run_ua_only.bat

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\data\prms_sce_calib\ua\gsflow_onlyPRMS_02_20220415\gsflow\output


6) Process/reformat the results from the three process-based models using:
   \ProcessBasedModels_SummmaryTables_Plots\src\04b_combine_model_results_updated.R

   Results can be found in the following folder:
   \ProcessBasedModels_SummmaryTables_Plots\results\04b_combine_model_results_updated


7) Copy the results from #6 
   (\ProcessBasedModels_SummmaryTables_Plots\results\04b_combine_model_results_updated\tables\alameda_data_for_lstm_prms_sacsma_ihacres_uncal_default_20220425.csv) 
   to the folders specified in the following ReadMe file:
   \LSTM_HybridModels_ErrorMetrics\code\README.txt.


8) Generate the LSTM and hybrid models.  Calculate the performance metrics.  
   Both these steps can be done by following the instructions in the following ReadMe file:
   \LSTM_HybridModels_ErrorMetrics\code\README.txt.


9) Copy the outputs from #8, found at locations specified in the following ReadMe file:
   \LSTM_HybridModels_ErrorMetrics\code\README.txt 
   to 
   \ProcessBasedModels_SummmaryTables_Plots\data\output_tables_ensemble_10_20221111 


10) Generate analysis plots and tables presented in the paper and supporting information by using the following scripts:
    - \ProcessBasedModels_SummmaryTables_Plots\src\11_plot_all_model_results_wetdry.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\11_plot_all_model_results.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\12_plot_all_model_results_paper.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\13_compare_models_by_research_question_reframed.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\14_create_summary_tables_reframed.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\15_plot_summary_boxplots.R
    - \ProcessBasedModels_SummmaryTables_Plots\src\16_make_pb_calibration_tables.R

   Results will be generated in the \ProcessBasedModels_SummmaryTables_Plots\results folder,
   inside of sub-folders that match the name of each R script listed above folder 
   (sub-folders are described in the directory structure below).




Directory Structure
-------------------
Below we describe the structure of the folder containing this ReadMe file, including 
sub-folders and files.



\ProcessBasedModels_SummmaryTables_Plots
----------------------------------------


	\data
	------

		Description:



		\output_tables_ensemble_10_20221111
		-----------------------------------

			Description:
			A folder containing the model performance metrics that were calculated on the process-based, LSTM, and hybrid models. See the 
			following ReadMe file for more information about these calculations:
			\LSTM_HybridModels_ErrorMetrics\code\README.txt 



			\Arroyo_Hondo
			-------------

				Description:
				A folder containing the model performance metrics that were calculated on the process-based, LSTM, and hybrid models for the 
				Arroyo Hondo watershed. See the following ReadMe file for more information about these calculations:
				\LSTM_HybridModels_ErrorMetrics\code\README.txt.  These model performance metrics are used by the following scripts to 
				generate plots and tables of the analysis results: 
					- 11_plot_all_model_results.R
					- 11_plot_all_model_results_wetdry.R


			\Upper_Alameda
			--------------

				Description:
				A folder containing the model performance metrics that were calculated on the process-based, LSTM, and hybrid models for the 
				Upper Alameda watershed. See the following ReadMe file for more information about these calculations:
				\LSTM_HybridModels_ErrorMetrics\code\README.txt.  These model performance metrics are used by the following scripts to 
				generate plots and tables of the analysis results: 	
					- 11_plot_all_model_results.R
					- 11_plot_all_model_results_wetdry.R			



		\prms_sce_calib
		---------------

			Description:
			Folder containing the calibrated PRMS models.


			\ah
			----

				Description:
				Folder containing the calibrated PRMS model for the Arroyo Hondo watershed.

				\gsflow_onlyPRMS_03_20220415
				----------------------------

					\gsflow
					-------

						Description: 
						Folder containing the calibrated PRMS model for the Arroyo Hondo watershed.

					\sce_calib
					----------

						Description: 
						Folder used for the Shuffled Complex Evolution calibration of the PRMS model for the Arroyo Hondo watershed.



			\ua
			----

				Description:
				Folder containing the calibrated PRMS model for the Upper Alameda watershed.

				\gsflow_onlyPRMS_02_20220415
				----------------------------

					\gsflow
					-------

						Description: 
						Folder containing the calibrated PRMS model for the Upper Alameda watershed.

					\sce_calib
					----------

						Description: 
						Folder used for the Shuffled Complex Evolution calibration of the PRMS model for the Upper Alameda watershed.



			\check_params
			--------------

				Description:
				A folder that can be used to check, read, write, and update PRMS parameters.

				\input

					Description: 
					Inputs to the utility scripts

				\output

					Description: 
					Outputs from the utility scripts

				\scripts
					Description:
					A folder containing utility scripts to check, read, write, and update PRMS parameters.




		\prms_uncal_default
		-------------------

			Description:
			Contains the uncalibrated PRMS model

			\gsflow
			--------

				\bin
				-----
					Description:
					Contains the PRMS executable file

				\input
				------
					Description:
					Contains the PRMS input files for the uncalibrated model

				\output
				-------
					Description:
					Contains the PRMS output files for the uncalibrated model

				prms_run_AH_UA.bat
					Description:
					Windows batch file used to run the uncalibrated PRMS model

				alameda_prms_updated_AH_UA.control
					Description:
					PRMS input file used to control model run options

				gsflow.log
					Description:
					PRMS output log file

				prms_ic.out
					Description:
					PRMS output file (not used)




			\prep_data_for_LSTM
				Description:
				This folder contains an R script (prep_data_for_LSTM.R) that reads in the output from the uncalibrated PRMS model and 
				generates a csv file (alameda_data_for_LSTM_prms_20211127.csv), which is an intermediate output that is used by \ProcessBasedModels_SummmaryTables_Plots\src\04b_combine_model_results_updated.R to prepare the process-based model results 
				for the LSTM.



		
		Files:

			alameda_data_for_LSTM_prms_20211127.csv
				Description: 
				An intermediate output that is used by \ProcessBasedModels_SummmaryTables_Plots\src\04b_combine_model_results_updated.R to prepare the process-based model results for the LSTM.

			ReadMe.txt
				Description: 
				A ReadMe file that describes the contents of alameda_data_for_LSTM_prms_20211127.csv

			




	\src
	-----

		Description:
		This folder contains the scripts used to run the IHACRES and SAC-SMA models, prepare 
		the process-based model outputs as inputs to the LSTM, and then make plots and tables
		of all (process-based, LSTM, and hybrid models) model results.  

		Files:

			01_ihacres.R
				Description: 
				Runs the IHACRES model

			02_sacsma.R
				Description: 
				Runs the SAC-SMA model

			04b_combine_model_results_updated.R
				Description: 
				Combines model results from IHACRES, SAC-SMA, and PRMS model outputs

			07_plot_california.R
				Description: 
				Generates a map of California that is presented in the paper

			11_plot_all_model_results_wetdry.R
				Description:
				Generates intermediate plots and tables of the analysis results.

			11_plot_all_model_results.R
				Description:
				Generates intermediate plots and tables of the analysis results.

			12_plot_all_model_results_paper.R
				Description:
				Generates intermediate plots and tables of the analysis results.

			13_compare_models_by_research_question_reframed.R
				Description:
				Generates plots of the analysis results that are presented in the 
				paper and in the Supporting Information.

			14_create_summary_tables_reframed.R
				Description:
				Generates summary tables that are presented in the Supporting Information
				for the paper.

			15_plot_summary_boxplots.R
				Description:
				Generates the summary boxplots that are presented in the paper.

			16_make_pb_calibration_tables.R
				Description:
				Generates tables of performance metrics for the uncalibrated 
				and calibrated process-based models during the calibration 
				and test periods that are presented in the Supporting Information 
				for the paper.




	\results
	--------

		Description:
		This folder contains the output generated by the scripts in the \ProcessBasedModels_SummmaryTables_Plots\src folder.


		\01_ihacres 
		------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\01_ihacres.R.


		\02_sacsma
		------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\02_sacsma.R.


		\04b_combine_model_results_updated
		-----------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\04b_combine_model_results_updated.R.


		\07_plot_california
		-------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\07_plot_california.R.


		\11_plot_all_model_results_wetdry
		--------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\11_plot_all_model_results_wetdry.R.


		\11_plot_all_model_results
		--------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\11_plot_all_model_results.R.


		\12_plot_all_model_results_paper
		--------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\12_plot_all_model_results_paper.R.


		\13_compare_models_by_research_question_reframed
		------------------------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\13_compare_models_by_research_question_reframed.R.


		\14_create_summary_tables_reframed
		----------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\14_create_summary_tables_reframed.R.


		\15_plot_summary_boxplots
		------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\15_plot_summary_boxplots.R.


		\16_make_pb_calibration_tables
		------------------------------
			Description:
			Contains output generated by \ProcessBasedModels_SummmaryTables_Plots\src\16_make_pb_calibration_tables.R.




	Files:

		ReadMe.txt
			Description:
			This is this ReadMe file.