#---- Goals --------------------------------------------------------------------------------------####

# To prepare data from uncalibrated PRMS model statvar files for LSTM


#---- Notes -----------------------------------------------------------------------####

# This script assumes that the statvar file has these columns:
# row
# year
# month
# day
# hour 
# minute
# second
# sub_cfs	1: IndianCreek
# sub_cfs	2: SanAntonioCreekAtIndianCreekRd
# sub_cfs	3: ArroyoHondo
# sub_cfs	4 SanAntonioCreek
# sub_cfs	5: CalaverasCreek
# sub_cfs	6: AlamedaCreekAboveACDD
# sub_cfs	7: AlamedaCreekBelowACDD
# sub_cfs	8: AlamedaCreekBelowCalaverasCreek
# sub_cfs	9: AlamedaCreekBelowWelchCreek
# sub_cfs	10: AlamedaCreekNearNiles
# sub_cfs	11: AlamedaCreekAboveSanAntonioCreek
# sub_cfs	12: AlamedaCreekAboveArroyoDeLaLaguna
# basin_cfs	1: WatershedExit
# runoff	1: IndianCreek
# runoff	2: SanAntonioCreekAtIndianCreekRd
# runoff	3: ArroyoHondo
# runoff	4: SanAntonioCreek
# runoff	5: CalaverasCreek
# runoff	6: AlamedaCreekAboveACDD
# runoff	7: AlamedaCreekBelowACDD
# runoff	8: AlamedaCreekBelowCalaverasCreek
# runoff	9: AlamedaCreekBelowWelchCreek
# runoff	10: AlamedaCreekNearNiles
# runoff	11: AlamedaCreekAboveSanAntonioCreek
# runoff	12: AlamedaCreekAboveArroyoDeLaLaguna
# runoff	13: ArroyoDeLaLagunaAtVerona
# subinc_precip	1: IndianCreek
# subinc_precip	2: SanAntonioCreekAtIndianCreekRd
# subinc_precip	3: ArroyoHondo
# subinc_precip	4: SanAntonioCreek
# subinc_precip	5: CalaverasCreek
# subinc_precip	6: AlamedaCreekAboveACDD
# subinc_precip	7: AlamedaCreekBelowACDD
# subinc_precip	8: AlamedaCreekBelowCalaverasCreek
# subinc_precip	9: AlamedaCreekBelowWelchCreek
# subinc_precip	10: AlamedaCreekNearNiles
# subinc_precip	11: AlamedaCreekAboveSanAntonioCreek
# subinc_precip	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_wb	1: IndianCreek
# subinc_wb	2: SanAntonioCreekAtIndianCreekRd
# subinc_wb	3: ArroyoHondo
# subinc_wb	4: SanAntonioCreek
# subinc_wb	5: CalaverasCreek
# subinc_wb	6: AlamedaCreekAboveACDD
# subinc_wb	7: AlamedaCreekBelowACDD
# subinc_wb	8: AlamedaCreekBelowCalaverasCreek
# subinc_wb	9: AlamedaCreekBelowWelchCreek
# subinc_wb	10: AlamedaCreekNearNiles
# subinc_wb	11: AlamedaCreekAboveSanAntonioCreek
# subinc_wb	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_deltastor	1: IndianCreek
# subinc_deltastor	2: SanAntonioCreekAtIndianCreekRd
# subinc_deltastor	3: ArroyoHondo
# subinc_deltastor	4: SanAntonioCreek
# subinc_deltastor	5: CalaverasCreek
# subinc_deltastor	6: AlamedaCreekAboveACDD
# subinc_deltastor	7: AlamedaCreekBelowACDD
# subinc_deltastor	8: AlamedaCreekBelowCalaverasCreek
# subinc_deltastor	9: AlamedaCreekBelowWelchCreek
# subinc_deltastor	10: AlamedaCreekNearNiles
# subinc_deltastor	11: AlamedaCreekAboveSanAntonioCreek
# subinc_deltastor	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_actet	1: IndianCreek
# subinc_actet	2: SanAntonioCreekAtIndianCreekRd
# subinc_actet	3: ArroyoHondo
# subinc_actet	4: SanAntonioCreek
# subinc_actet	5: CalaverasCreek
# subinc_actet	6: AlamedaCreekAboveACDD
# subinc_actet	7: AlamedaCreekBelowACDD
# subinc_actet	8: AlamedaCreekBelowCalaverasCreek
# subinc_actet	9: AlamedaCreekBelowWelchCreek
# subinc_actet	10: AlamedaCreekNearNiles
# subinc_actet	11: AlamedaCreekAboveSanAntonioCreek
# subinc_actet	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_potet	1: IndianCreek
# subinc_potet	2: SanAntonioCreekAtIndianCreekRd
# subinc_potet	3: ArroyoHondo
# subinc_potet	4: SanAntonioCreek
# subinc_potet	5: CalaverasCreek
# subinc_potet	6: AlamedaCreekAboveACDD
# subinc_potet	7: AlamedaCreekBelowACDD
# subinc_potet	8: AlamedaCreekBelowCalaverasCreek
# subinc_potet	9: AlamedaCreekBelowWelchCreek
# subinc_potet	10: AlamedaCreekNearNiles
# subinc_potet	11: AlamedaCreekAboveSanAntonioCreek
# subinc_potet	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_swrad	1: IndianCreek
# subinc_swrad	2: SanAntonioCreekAtIndianCreekRd
# subinc_swrad	3: ArroyoHondo
# subinc_swrad	4: SanAntonioCreek
# subinc_swrad	5: CalaverasCreek
# subinc_swrad	6: AlamedaCreekAboveACDD
# subinc_swrad	7: AlamedaCreekBelowACDD
# subinc_swrad	8: AlamedaCreekBelowCalaverasCreek
# subinc_swrad	9: AlamedaCreekBelowWelchCreek
# subinc_swrad	10: AlamedaCreekNearNiles
# subinc_swrad	11: AlamedaCreekAboveSanAntonioCreek
# subinc_swrad	12: AlamedaCreekAboveArroyoDeLaLaguna
# sub_inq	1: IndianCreek
# sub_inq	2: SanAntonioCreekAtIndianCreekRd
# sub_inq	3: ArroyoHondo
# sub_inq	4: SanAntonioCreek
# sub_inq	5: CalaverasCreek
# sub_inq	6: AlamedaCreekAboveACDD
# sub_inq	7: AlamedaCreekBelowACDD
# sub_inq	8: AlamedaCreekBelowCalaverasCreek
# sub_inq	9: AlamedaCreekBelowWelchCreek
# sub_inq	10: AlamedaCreekNearNiles
# sub_inq	11: AlamedaCreekAboveSanAntonioCreek
# sub_inq	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_interflow	1: IndianCreek
# subinc_interflow	2: SanAntonioCreekAtIndianCreekRd
# subinc_interflow	3: ArroyoHondo
# subinc_interflow	4: SanAntonioCreek
# subinc_interflow	5: CalaverasCreek
# subinc_interflow	6: AlamedaCreekAboveACDD
# subinc_interflow	7: AlamedaCreekBelowACDD
# subinc_interflow	8: AlamedaCreekBelowCalaverasCreek
# subinc_interflow	9: AlamedaCreekBelowWelchCreek
# subinc_interflow	10: AlamedaCreekNearNiles
# subinc_interflow	11: AlamedaCreekAboveSanAntonioCreek
# subinc_interflow	12: AlamedaCreekAboveArroyoDeLaLaguna
# sub_interflow	1: IndianCreek
# sub_interflow	2: SanAntonioCreekAtIndianCreekRd
# sub_interflow	3: ArroyoHondo
# sub_interflow	4: SanAntonioCreek
# sub_interflow	5: CalaverasCreek
# sub_interflow	6: AlamedaCreekAboveACDD
# sub_interflow	7: AlamedaCreekBelowACDD
# sub_interflow	8: AlamedaCreekBelowCalaverasCreek
# sub_interflow	9: AlamedaCreekBelowWelchCreek
# sub_interflow	10: AlamedaCreekNearNiles
# sub_interflow	11: AlamedaCreekAboveSanAntonioCreek
# sub_interflow	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_sroff	1: IndianCreek
# subinc_sroff	2: SanAntonioCreekAtIndianCreekRd
# subinc_sroff	3: ArroyoHondo
# subinc_sroff	4: SanAntonioCreek
# subinc_sroff	5: CalaverasCreek
# subinc_sroff	6: AlamedaCreekAboveACDD
# subinc_sroff	7: AlamedaCreekBelowACDD
# subinc_sroff	8: AlamedaCreekBelowCalaverasCreek
# subinc_sroff	9: AlamedaCreekBelowWelchCreek
# subinc_sroff	10: AlamedaCreekNearNiles
# subinc_sroff	11: AlamedaCreekAboveSanAntonioCreek
# subinc_sroff	12: AlamedaCreekAboveArroyoDeLaLaguna
# sub_sroff	1: IndianCreek
# sub_sroff	2: SanAntonioCreekAtIndianCreekRd
# sub_sroff	3: ArroyoHondo
# sub_sroff	4: SanAntonioCreek
# sub_sroff	5: CalaverasCreek
# sub_sroff	6: AlamedaCreekAboveACDD
# sub_sroff	7: AlamedaCreekBelowACDD
# sub_sroff	8: AlamedaCreekBelowCalaverasCreek
# sub_sroff	9: AlamedaCreekBelowWelchCreek
# sub_sroff	10: AlamedaCreekNearNiles
# sub_sroff	11: AlamedaCreekAboveSanAntonioCreek
# sub_sroff	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_gwflow	1: IndianCreek
# subinc_gwflow	2: SanAntonioCreekAtIndianCreekRd
# subinc_gwflow	3: ArroyoHondo
# subinc_gwflow	4: SanAntonioCreek
# subinc_gwflow	5: CalaverasCreek
# subinc_gwflow	6: AlamedaCreekAboveACDD
# subinc_gwflow	7: AlamedaCreekBelowACDD
# subinc_gwflow	8: AlamedaCreekBelowCalaverasCreek
# subinc_gwflow	9: AlamedaCreekBelowWelchCreek
# subinc_gwflow	10: AlamedaCreekNearNiles
# subinc_gwflow	11: AlamedaCreekAboveSanAntonioCreek
# subinc_gwflow	12: AlamedaCreekAboveArroyoDeLaLaguna
# sub_gwflow	1: IndianCreek
# sub_gwflow	2: SanAntonioCreekAtIndianCreekRd
# sub_gwflow	3: ArroyoHondo
# sub_gwflow	4: SanAntonioCreek
# sub_gwflow	5: CalaverasCreek
# sub_gwflow	6: AlamedaCreekAboveACDD
# sub_gwflow	7: AlamedaCreekBelowACDD
# sub_gwflow	8: AlamedaCreekBelowCalaverasCreek
# sub_gwflow	9: AlamedaCreekBelowWelchCreek
# sub_gwflow	10: AlamedaCreekNearNiles
# sub_gwflow	11: AlamedaCreekAboveSanAntonioCreek
# sub_gwflow	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_tavgc	1: IndianCreek
# subinc_tavgc	2: SanAntonioCreekAtIndianCreekRd
# subinc_tavgc	3: ArroyoHondo
# subinc_tavgc	4: SanAntonioCreek
# subinc_tavgc	5: CalaverasCreek
# subinc_tavgc	6: AlamedaCreekAboveACDD
# subinc_tavgc	7: AlamedaCreekBelowACDD
# subinc_tavgc	8: AlamedaCreekBelowCalaverasCreek
# subinc_tavgc	9: AlamedaCreekBelowWelchCreek
# subinc_tavgc	10: AlamedaCreekNearNiles
# subinc_tavgc	11: AlamedaCreekAboveSanAntonioCreek
# subinc_tavgc	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_tmaxc	1: IndianCreek
# subinc_tmaxc	2: SanAntonioCreekAtIndianCreekRd
# subinc_tmaxc	3: ArroyoHondo
# subinc_tmaxc	4: SanAntonioCreek
# subinc_tmaxc	5: CalaverasCreek
# subinc_tmaxc	6: AlamedaCreekAboveACDD
# subinc_tmaxc	7: AlamedaCreekBelowACDD
# subinc_tmaxc	8: AlamedaCreekBelowCalaverasCreek
# subinc_tmaxc	9: AlamedaCreekBelowWelchCreek
# subinc_tmaxc	10: AlamedaCreekNearNiles
# subinc_tmaxc	11: AlamedaCreekAboveSanAntonioCreek
# subinc_tmaxc	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_tminc	1: IndianCreek
# subinc_tminc	2: SanAntonioCreekAtIndianCreekRd
# subinc_tminc	3: ArroyoHondo
# subinc_tminc	4: SanAntonioCreek
# subinc_tminc	5: CalaverasCreek
# subinc_tminc	6: AlamedaCreekAboveACDD
# subinc_tminc	7: AlamedaCreekBelowACDD
# subinc_tminc	8: AlamedaCreekBelowCalaverasCreek
# subinc_tminc	9: AlamedaCreekBelowWelchCreek
# subinc_tminc	10: AlamedaCreekNearNiles
# subinc_tminc	11: AlamedaCreekAboveSanAntonioCreek
# subinc_tminc	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_pkweqv	1: IndianCreek
# subinc_pkweqv	2: SanAntonioCreekAtIndianCreekRd
# subinc_pkweqv	3: ArroyoHondo
# subinc_pkweqv	4: SanAntonioCreek
# subinc_pkweqv	5: CalaverasCreek
# subinc_pkweqv	6: AlamedaCreekAboveACDD
# subinc_pkweqv	7: AlamedaCreekBelowACDD
# subinc_pkweqv	8: AlamedaCreekBelowCalaverasCreek
# subinc_pkweqv	9: AlamedaCreekBelowWelchCreek
# subinc_pkweqv	10: AlamedaCreekNearNiles
# subinc_pkweqv	11: AlamedaCreekAboveSanAntonioCreek
# subinc_pkweqv	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_snowcov	1: IndianCreek
# subinc_snowcov	2: SanAntonioCreekAtIndianCreekRd
# subinc_snowcov	3: ArroyoHondo
# subinc_snowcov	4: SanAntonioCreek
# subinc_snowcov	5: CalaverasCreek
# subinc_snowcov	6: AlamedaCreekAboveACDD
# subinc_snowcov	7: AlamedaCreekBelowACDD
# subinc_snowcov	8: AlamedaCreekBelowCalaverasCreek
# subinc_snowcov	9: AlamedaCreekBelowWelchCreek
# subinc_snowcov	10: AlamedaCreekNearNiles
# subinc_snowcov	11: AlamedaCreekAboveSanAntonioCreek
# subinc_snowcov	12: AlamedaCreekAboveArroyoDeLaLaguna
# subinc_snowmelt	1: IndianCreek
# subinc_snowmelt	2: SanAntonioCreekAtIndianCreekRd
# subinc_snowmelt	3: ArroyoHondo
# subinc_snowmelt	4: SanAntonioCreek
# subinc_snowmelt	5: CalaverasCreek
# subinc_snowmelt	6: AlamedaCreekAboveACDD
# subinc_snowmelt	7: AlamedaCreekBelowACDD
# subinc_snowmelt	8: AlamedaCreekBelowCalaverasCreek
# subinc_snowmelt	9: AlamedaCreekBelowWelchCreek
# subinc_snowmelt	10: AlamedaCreekNearNiles
# subinc_snowmelt	11: AlamedaCreekAboveSanAntonioCreek
# subinc_snowmelt	12: AlamedaCreekAboveArroyoDeLaLaguna

# In the future, the simplest way to add on additional outputs to the 
# statvar file (and therefore to the plotting performed by this script)
# is to add on the new output variables to the end of the list above, 
# without changing the order of the variables above.  This way, none 
# of the indices in the existing script will need to be changed.


#---- Set up R environment -----------------------------------------------------------------------####

# set working directory
setwd(here('data', 'prms_uncal_default', 'gsflow_onlyPRMS', 'prep_data_for_LSTM')) 

#install packages
# install.packages("plyr")
# install.packages("tidyverse")
# install.packages("broom")
# install.packages("lubridate")
# install.packages("naniar")
# install.packages("zoo")

#load libraries
library(plyr)
library(tidyverse)
library(broom)
library(lubridate)
library(naniar)
library(zoo)



#---- Source functions ----------------------------------------------------------------------------####


#---- Choose program settings -----------------------------------------------------------------------####


#---- Set constants ---------------------------------------------------------------------------------####

# Set values of subbasin0#_OnOff to 0 or 1
# 0 = don't plot, 1 = plot
subbasin01_OnOff <- 0 # Indian Creek
subbasin02_OnOff <- 0 # San Antonio Creek at Indian Creek Rd
subbasin03_OnOff <- 1 # Arroyo Hondo
subbasin04_OnOff <- 0 # San Antonio Creek
subbasin05_OnOff <- 0 # Calaveras Creek
subbasin06_OnOff <- 1 # Alameda Creek above ACDD
subbasin07_OnOff <- 0 # Alameda Creek below ACDD
subbasin08_OnOff <- 0 # Alameda Creek below Calaveras Creek
subbasin09_OnOff <- 0 # Alameda Creek below Welch Creek
subbasin10_OnOff <- 0 # Alameda Creek near Niles
subbasin11_OnOff <- 0 # Alameda Creek above San Antonio Creek
subbasin12_OnOff <- 0 # Alameda Creek above Arroyo de la Laguna

subbasin_OnOff <- c(subbasin01_OnOff,
                    subbasin02_OnOff,
                    subbasin03_OnOff,
                    subbasin04_OnOff,
                    subbasin05_OnOff,
                    subbasin06_OnOff,
                    subbasin07_OnOff,
                    subbasin08_OnOff,
                    subbasin09_OnOff,
                    subbasin10_OnOff,
                    subbasin11_OnOff,
                    subbasin12_OnOff)


# assign subbasin names
subbasinNames <- c("IndianCreek", 
                   "SanAntonioCreekAtIndianCreekRd",
                   "ArroyoHondo",	
                   "SanAntonioCreek",	
                   "CalaverasCreek",
                   "AlamedaCreekAboveACDD",	
                   "AlamedaCreekBelowACDD",
                   "AlamedaCreekBelowCalaverasCreek",
                   "AlamedaCreekBelowWelchCreek",
                   "AlamedaCreekNearNiles",
                   "AlamedaCreekAboveSanAntonioCreek",
                   "AlamedaCreekAboveArroyoDeLaLaguna")


subbasinNamesPretty <- c("Indian Creek", 
                         "San Antonio Creek At Indian Creek Rd",
                         "Arroyo Hondo",	
                         "San Antonio Creek",	
                         "Calaveras Creek",
                         "Alameda Creek Above ACDD",	
                         "Alameda Creek Below ACDD",
                         "Alameda Creek Below Calaveras Creek",
                         "Alameda Creek Below Welch Creek",
                         "Alameda Creek Near Niles",
                         "Alameda Creek Above San Antonio Creek",
                         "Alameda Creek Above Arroyo De La Laguna")


# prep subbasin indices for variable data frames
numDateCol <- 7
subbasinIdxTmp <- numDateCol + which(subbasin_OnOff == 1)
subbasinIdx <- c(1:numDateCol, subbasinIdxTmp)

# choose subbasin variables
subbasin_var_vec <- c("pref_flow_stor", "slow_stor", "soil_moist", "soil_moist_tot", 
                      "hru_intcpstor", "hru_storage", "gwres_stor", "dunnian_flow", 
                      "hortonian_flow")




#---- Choose statvar input data path ----------------------------------------------------------------####

uncalib_path <- "../gsflow/output/prms/uncalibrated/"


#---- Choose output paths -----------------------------------------------------------------------####



#---- Read in statvar files ---------------------------------------------------------------------####

# create list to store statvar files
statvarReadIn_list <- vector(mode="list", length=1)

# read in uncalibrated statvar file
filePath <- paste0(uncalib_path, "/statvar.csv")
statvarReadIn_list[[1]] <- read.csv(filePath, header=FALSE, na.strings="-999", stringsAsFactors=FALSE)

# set names of statvarReadIn_list
names(statvarReadIn_list) <- c("prms_uncalib")




#---- Read in files from new subbasin module ----------------------------------------------------####

# read in outputs from new subbasin module: uncalibrated
folderPath_uncalib <- paste0(uncalib_path, "nsubOutVar")
filenames_uncalib <- list.files(path = folderPath_uncalib, pattern="*.csv")
filenamesPath_uncalib <- paste(uncalib_path, "nsubOutVar/", filenames_uncalib, sep="")
subbasinVar_uncalib = lapply(filenamesPath_uncalib, read.csv, header=TRUE)

# extract variable names from filenames: uncalibrated
variableNamesTmp_uncalib <- sapply(filenames_uncalib, strsplit, split="\\.")
subbasinVarNames_uncalib <- as.character(sapply(variableNamesTmp_uncalib, "[[",2))





#---- Reformat statvar data -----------------------------------------------------------------------####

# prep variables to extract from each statvar file in loop
uncalib_var <- c("date", "ArroyoHondo", "AlamedaCreekAboveACDD")
var_to_extract <- list(uncalib_var)


# prep subbasin ids for loop
sub_id <- c("AH", "UA")

# loop through statvar files
sub_cfs_list <- vector(mode="list", length=length(statvarReadIn_list))
names(sub_cfs_list) <- names(statvarReadIn_list)
subinc_actet_list <- sub_cfs_list
subinc_potet_list <- sub_cfs_list
subinc_swrad_list <- sub_cfs_list
sub_interflow_list <- sub_cfs_list
sub_sroff_list <- sub_cfs_list
sub_gwflow_list <- sub_cfs_list
subinc_precip_list <- vector(mode="list", length=length(statvarReadIn_list))
names(subinc_precip_list) <- c("obs")
subinc_tmaxc_list <- subinc_precip_list
subinc_tminc_list <- subinc_precip_list
runoff_list <- subinc_precip_list
for (i in 1:length(statvarReadIn_list)){
  
  # extract statvar file
  statvarReadIn <- statvarReadIn_list[[i]]
  
  # set statvar headers aside
  numVar <- as.numeric(statvarReadIn[1,1])
  numHeaderLines <- numVar + 1
  headerLines <- statvarReadIn[ (1:numHeaderLines), (1:2) ]
  statvar <- statvarReadIn[ (numHeaderLines+1): nrow(statvarReadIn), ]
  
  # asssign column names
  names(statvar) <- c("row",
                      "year",
                      "month",
                      "day",
                      "hour",
                      "minute",
                      "second",
                      "IndianCreek", 
                      "SanAntonioCreekAtIndianCreekRd",
                      "ArroyoHondo",	
                      "SanAntonioCreek",	
                      "CalaverasCreek",
                      "AlamedaCreekAboveACDD",	
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles",
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",	
                      "WatershedExit",
                      "IndianCreek", 
                      "SanAntonioCreekAtIndianCreekRd",
                      "ArroyoHondo",	
                      "SanAntonioCreek",	
                      "CalaverasCreek",
                      "AlamedaCreekAboveACDD",	
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles",
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "ArroyoDeLaLagunaAtVerona", 
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna",
                      "IndianCreek",
                      "SanAntonioCreekAtIndianCreekRd",	
                      "ArroyoHondo",
                      "SanAntonioCreek", 
                      "CalaverasCreek", 
                      "AlamedaCreekAboveACDD", 
                      "AlamedaCreekBelowACDD",
                      "AlamedaCreekBelowCalaverasCreek",
                      "AlamedaCreekBelowWelchCreek",
                      "AlamedaCreekNearNiles", 
                      "AlamedaCreekAboveSanAntonioCreek",
                      "AlamedaCreekAboveArroyoDeLaLaguna")
  
  
  # create datetime column
  # need to change date column to pacific time zone
  date <- as.Date(ISOdatetime(statvar$year, statvar$month, statvar$day, 
                              statvar$hour, statvar$minute, statvar$second, 
                              tz="UTC"))
  
  # create YMDHMS data frame
  ymdhms <- statvar[,2:7]
  
  # pull out individual statvar variables, add date in
  sub_cfs <- cbind(date, statvar[,2:19])  # units: cfs
  basin_cfs <- cbind(date, statvar[,20])  # units: cfs
  runoff <- cbind(date, statvar[,c(2:7,21:33)])   # units: cfs
  subinc_precip <- cbind(date, statvar[, c(2:7,34:45)])   # units: inches
  subinc_wb <- cbind(date, statvar[, c(2:7,46:57)])     # units: cfs
  subinc_deltastor <- cbind(date, statvar[, c(2:7,58:69)])   # units: cfs
  subinc_actet <- cbind(date, statvar[, c(2:7,70:81)])    # units: inches
  subinc_potet <- cbind(date, statvar[, c(2:7,82:93)])    # units: inches
  subinc_swrad <- cbind(date, statvar[, c(2:7,94:105)])   # units: Langleys = J / m^2
  sub_inq <- cbind(date, statvar[, c(2:7,106:117)])      # units: cfs
  subinc_interflow <- cbind(date, statvar[, c(2:7,118:129)])      # units: cfs
  sub_interflow <- cbind(date, statvar[, c(2:7,130:141)])      # units: cfs
  subinc_sroff <- cbind(date, statvar[, c(2:7,142:153)])      # units: cfs
  sub_sroff <- cbind(date, statvar[, c(2:7,154:165)])      # units: cfs
  subinc_gwflow <- cbind(date, statvar[, c(2:7,166:177)])      # units: cfs
  sub_gwflow <- cbind(date, statvar[, c(2:7,178:189)])      # units: cfs
  subinc_tavgc <- cbind(date, statvar[, c(2:7,190:201)])      # units: degrees Celsius
  subinc_tmaxc <- cbind(date, statvar[, c(2:7,202:213)])      # units: degrees Celsius
  subinc_tminc <- cbind(date, statvar[, c(2:7,214:225)])      # units: degrees Celsius
  subinc_pkweqv <- cbind(date, statvar[, c(2:7,226:237)])      # units: inches
  subinc_snowcov <- cbind(date, statvar[, c(2:7,238:249)])      # units: decimal fraction
  subinc_snowmelt <- cbind(date, statvar[, c(2:7,250:261)])      # units: inches
  

  # store in list
  sub_cfs_list[[i]] <- sub_cfs[, var_to_extract[[i]]]
  subinc_precip_list[[i]] <- subinc_precip[, var_to_extract[[i]]]
  subinc_tmaxc_list[[i]] <- subinc_tmaxc[, var_to_extract[[i]]]
  subinc_tminc_list[[i]] <- subinc_tminc[, var_to_extract[[i]]]
  runoff_list[[i]] <- runoff[, var_to_extract[[i]]]
  subinc_actet_list[[i]] <- subinc_actet[, var_to_extract[[i]]]
  subinc_potet_list[[i]] <- subinc_potet[, var_to_extract[[i]]]
  subinc_swrad_list[[i]] <- subinc_swrad[, var_to_extract[[i]]]
  sub_interflow_list[[i]] <- sub_interflow[, var_to_extract[[i]]]
  sub_sroff_list[[i]] <- sub_sroff[, var_to_extract[[i]]]
  sub_gwflow_list[[i]] <- sub_gwflow[, var_to_extract[[i]]]
  
  
  # set names  
  names(sub_cfs_list[[i]]) <- c("date", 
                                paste0(names(sub_cfs_list)[i], "_sim_flow_AH"), 
                                paste0(names(sub_cfs_list)[i], "_sim_flow_UA"))
  names(subinc_actet_list[[i]]) <- c("date", 
                                     paste0(names(sub_cfs_list)[i], "_sim_actet_AH"), 
                                     paste0(names(sub_cfs_list)[i], "_sim_actet_UA"))
  names(subinc_potet_list[[i]]) <- c("date", 
                                     paste0(names(subinc_potet_list)[i], "_sim_potet_AH"), 
                                     paste0(names(subinc_potet_list)[i], "_sim_potet_UA"))
  names(subinc_swrad_list[[i]]) <- c("date", 
                                     paste0(names(subinc_swrad_list)[i], "_sim_swrad_AH"), 
                                     paste0(names(subinc_swrad_list)[i], "_sim_swrad_UA"))
  names(sub_interflow_list[[i]]) <- c("date", 
                                      paste0(names(sub_interflow_list)[i], "_sim_interflow_AH"), 
                                      paste0(names(sub_interflow_list)[i], "_sim_interflow_UA"))
  names(sub_sroff_list[[i]]) <- c("date", 
                                  paste0(names(sub_sroff_list)[i], "_sim_sroff_AH"), 
                                  paste0(names(sub_sroff_list)[i], "_sim_sroff_UA"))
  names(sub_gwflow_list[[i]]) <- c("date", 
                                   paste0(names(sub_gwflow_list)[i], "_sim_gwflow_AH"), 
                                   paste0(names(sub_gwflow_list)[i], "_sim_gwflow_UA"))
  names(subinc_precip_list[[i]]) <- c("date", 
                                      "obs_precip_AH",
                                      "obs_precip_UA")
  names(subinc_tmaxc_list[[i]]) <- c("date", 
                                     "obs_tmaxc_AH",
                                     "obs_tmaxc_UA")
  names(subinc_tminc_list[[i]]) <- c("date", 
                                     "obs_tminc_AH",
                                     "obs_tminc_UA")
  names(runoff_list[[i]]) <- c("date", 
                               "obs_flow_AH",
                               "obs_flow_UA")
    
  
}


# join
sim_flow <- sub_cfs_list %>% reduce(left_join, by="date")
sim_actet <- subinc_actet_list %>% reduce(left_join, by="date")
sim_potet <- subinc_potet_list %>% reduce(left_join, by="date")
sim_swrad <- subinc_swrad_list %>% reduce(left_join, by="date")
sim_interflow <- sub_interflow_list %>% reduce(left_join, by="date")
sim_sroff <- sub_sroff_list %>% reduce(left_join, by="date")
sim_gwflow <- sub_gwflow_list %>% reduce(left_join, by="date")
obs_precip <- subinc_precip_list[1] %>% reduce(left_join, by="date")
obs_tmax <- subinc_tmaxc_list[1] %>% reduce(left_join, by="date")
obs_tmin <- subinc_tminc_list[1] %>% reduce(left_join, by="date")
obs_flow <- runoff_list[1] %>% reduce(left_join, by="date")
data_for_LSTM <- list(obs_precip,
                      obs_tmax,
                      obs_tmin,
                      obs_flow,
                      sim_flow,
                      sim_actet,
                      sim_potet,
                      sim_swrad,
                      sim_interflow,
                      sim_sroff,
                      sim_gwflow) %>% reduce(left_join, by="date")







#---- Reformat data from new subbasin module ----------------------------------------------------####


# reformat subbasinVar_uncalib
names(subbasinVar_uncalib) <- subbasinVarNames_uncalib
subbasinVar_uncalib <- subbasinVar_uncalib[subbasin_var_vec]
for (i in 1: length(subbasinVar_uncalib)){

  # rename columns
  names(subbasinVar_uncalib[[i]]) <- c("date", subbasinNames)
  subbasinVar_uncalib[[i]]$date <- ymd(subbasinVar_uncalib[[i]]$date)

  # select columns
  subbasinVar_uncalib[[i]] <- subbasinVar_uncalib[[i]][, c("date", "ArroyoHondo", "AlamedaCreekAboveACDD")]

  # rename columns again
  names(subbasinVar_uncalib[[i]]) <- c("date",
                                     paste0("prms_uncalib_sim_", names(subbasinVar_uncalib)[i], "_AH"),
                                     paste0("prms_uncalib_sim_", names(subbasinVar_uncalib)[i], "_UA"))

}
nsub_prms_uncalib <- left_join(subbasinVar_uncalib$pref_flow_stor,
                             subbasinVar_uncalib$slow_stor, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$soil_moist, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$soil_moist_tot, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$hru_intcpstor, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$hru_storage, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$gwres_stor, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$dunnian_flow, by=c("date"))
nsub_prms_uncalib <- left_join(nsub_prms_uncalib, subbasinVar_uncalib$hortonian_flow, by=c("date"))


# combine data for LSTM 
data_for_LSTM <- list(data_for_LSTM, nsub_prms_uncalib) %>%
  reduce(left_join, by="date")


# reorder columns
obs_AH_cols <- which(grepl(pattern = "obs_", names(data_for_LSTM)) & grepl(pattern = "_AH", names(data_for_LSTM)))
obs_UA_cols <- which(grepl(pattern = "obs_", names(data_for_LSTM)) & grepl(pattern = "_UA", names(data_for_LSTM)))
uncalib_AH_cols <- which(grepl(pattern = "_uncalib_", names(data_for_LSTM)) & grepl(pattern = "_AH", names(data_for_LSTM)))
uncalib_UA_cols <- which(grepl(pattern = "_uncalib_", names(data_for_LSTM)) & grepl(pattern = "_UA", names(data_for_LSTM)))
data_for_LSTM <- data_for_LSTM[, c(1, 
                                   obs_AH_cols,
                                   uncalib_AH_cols,
                                   obs_UA_cols,
                                   uncalib_UA_cols)]




#---- Export ----------------------------------------------------------------------------------####

write.csv(data_for_LSTM, file="alameda_data_for_LSTM_prms_20211127.csv", row.names=FALSE)


