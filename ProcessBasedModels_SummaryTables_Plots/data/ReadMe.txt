This is an explanation of the columns in the csv file called alameda_data_for_lstm_prms_20211127.csv



Column 1 is the date
1) date: date 



Columns 2-5 are observed Arroyo Hondo subbasin variables
2) obs_precip_AH: observed precipitation (inches) in Arroyo Hondo subbasin
3) obs_tmaxc_AH: observed maximum temperature (degrees Celsius) in Arroyo Hondo subbasin
4) obs_tminc_AH: observed minimum temperature (degrees Celsius) in Arroyo Hondo subbasin
5) obs_flow_AH: observed streamflow (cfs) in Arroyo Hondo subbasin


Columns 6-21 are simulated Arroyo Hondo subbasin variables from an uncalibrated PRMS model
6) prms_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
7) prms_uncalib_sim_actet_AH: simulated actual ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
8) prms_uncalib_sim_potet_AH: simulated potential ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
9) prms_uncalib_sim_swrad_AH: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Arroyo Hondo subbasin
10) prms_uncalib_sim_interflow_AH: simulated interflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
11) prms_uncalib_sim_sroff_AH: simulated surface runoff (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
12) prms_uncalib_sim_gwflow_AH: simulated groundwater flow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
13) prms_uncalib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
14) prms_uncalib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
15) prms_uncalib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
16) prms_uncalib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
17) prms_uncalib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Arroyo Hondo subbasin
18) prms_uncalib_sim_hru_storage_AH: total simulated storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
19) prms_uncalib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Arroyo Hondo subbasin
20) prms_uncalib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
21) prms_uncalib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin


Columns 22-37 are simulated Arroyo Hondo subbasin variables from a calibrated PRMS model
22) prms_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
23) prms_calib_sim_actet_AH: simulated actual ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
24) prms_calib_sim_potet_AH: simulated potential ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
25) prms_calib_sim_swrad_AH: simulated shortwave radiation (Langleys) from calibrated PRMS model in Arroyo Hondo subbasin
26) prms_calib_sim_interflow_AH: simulated interflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
27) prms_calib_sim_sroff_AH: simulated surface runoff (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
28) prms_calib_sim_gwflow_AH: simulated groundwater flow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
29) prms_calib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
30) prms_calib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
31) prms_calib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
32) prms_calib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
33) prms_calib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from calibrated PRMS model in Arroyo Hondo subbasin
34) prms_calib_sim_hru_storage_AH: total simulated storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
35) prms_calib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Arroyo Hondo subbasin
36) prms_calib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
37) prms_calib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin



Columns 38-41 are observed Upper Alameda subbasin variables
38) obs_precip_UA: observed precipitation (inches) in Upper Alameda subbasin
39) obs_tmaxc_UA: observed maximum temperature (degrees Celsius) in Upper Alameda subbasin
40) obs_tminc_UA: observed minimum temperature (degrees Celsius) in Upper Alameda subbasin
41) obs_flow_UA: observed streamflow (cfs) in Upper Alameda subbasin



Columns 42-57 are simulated Upper Alameda subbasin variables from an uncalibrated PRMS model
42) prms_uncalib_sim_flow_UA: simulated streamflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
43) prms_uncalib_sim_actet_UA: simulated actual ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
44) prms_uncalib_sim_potet_UA: simulated potential ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
45) prms_uncalib_sim_swrad_UA: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Upper Alameda subbasin
46) prms_uncalib_sim_interflow_UA: simulated interflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
47) prms_uncalib_sim_sroff_UA: simulated surface runoff (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
48) prms_uncalib_sim_gwflow_UA: simulated groundwater flow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
49) prms_uncalib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
50) prms_uncalib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
51) prms_uncalib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
52) prms_uncalib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
53) prms_uncalib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Upper Alameda subbasin
54) prms_uncalib_sim_hru_storage_UA: total simulated storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
55) prms_uncalib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Upper Alameda subbasin
56) prms_uncalib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin
57) prms_uncalib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin



Columns 58-73 are simulated Upper Alameda subbasin variables from a calibrated PRMS model
58) prms_calib_sim_flow_UA: simulated streamflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
59) prms_calib_sim_actet_UA: simulated actual ET (inches) from calibrated PRMS model in Upper Alameda subbasin
60) prms_calib_sim_potet_UA: simulated potential ET (inches) from calibrated PRMS model in Upper Alameda subbasin
61) prms_calib_sim_swrad_UA: simulated shortwave radiation (Langleys) from calibrated PRMS model in Upper Alameda subbasin
62) prms_calib_sim_interflow_UA: simulated interflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
63) prms_calib_sim_sroff_UA: simulated surface runoff (cfs) from calibrated PRMS model in Upper Alameda subbasin
64) prms_calib_sim_gwflow_UA: simulated groundwater flow (cfs) from calibrated PRMS model in Upper Alameda subbasin
65) prms_calib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
66) prms_calib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
67) prms_calib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
68) prms_calib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from calibrated PRMS model in Upper Alameda subbasin
69) prms_calib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from calibrated PRMS model in Upper Alameda subbasin
70) prms_calib_sim_hru_storage_UA: total simulated storage (inches) from calibrated PRMS model in Upper Alameda subbasin
71) prms_calib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Upper Alameda subbasin
72) prms_calib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin
73) prms_calib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin
