# Goal ----------------------------------------------------####

# To update PRMS parameters with calibrated parameters



# Working directory ------------------------------------------------------------------####

# Set working directory for this script
script_dir <- "C:/workspace/report_prep/models/gsflow_onlyPRMS_03_20220415/sce_calib/scripts"
setwd(script_dir)

# Set project dir
project_dir <- "C:/workspace/report_prep/models/gsflow_onlyPRMS_03_20220415"



# Packages ---------------------------------------------------------------####

# install
# install.packages("tidyverse")

# load
library(tidyverse)


# Load other scripts ---------------------------------------------------------------####

source("read_prms_parms.R")
source("write_prms_parms.R")



# Settings ---------------------------------------------------------------####

# set calibrated parameter file
#calib_model_file <- paste0(project_dir, '/sce_calib/output/calib_model_ah.RData')

# set subbasin name
subbasin_name <- "ah"

# set prms param file paths
prms_param_file_in <- paste0(project_dir,
                             "/gsflow/input/prms/alameda_calibration_parameters_", subbasin_name, "_uncal_only.param")
prms_param_file_out <- paste0(project_dir,
                              "/gsflow/input/prms/alameda_calibration_parameters_", subbasin_name, "_uncal_only_updated.param")

# set model run batch file path
model_run_batch_file <- paste0(project_dir, "/gsflow/prms_run_", subbasin_name, "_only.bat")

# set options
get_calib_param <- 1

# # set calibrated params
# calib_param <- list(fastcoef_sq = , 
#                     pref_flow_den = , 
#                     slowcoef_sq = ,
#                     soil_moist_max = ,
#                     soil2gw_max = ,
#                     fastcoef_lin = ,
#                     gwflow_coef = ,
#                     jh_coef = ,
#                     slowcoef_lin = ,
#                     sat_threshold = ,
#                     carea_max = ,
#                     smidx_coef = ,
#                     smidx_exp = )





# Identify best parameter values ---------------------------------------------------------------####

if (get_calib_param == 1){
  
  # load calibrated parameter file - not working for some reason
  #calib_model <- load(calib_model_file)
  
  # get pop fit files
  pop_fit_folder <- paste0(project_dir, "/sce_calib/output/pop_fit/")
  pop_fit_files_partial <- list.files(path = pop_fit_folder, full.names = FALSE)
  pop_fit_files_full <- list.files(path = pop_fit_folder, full.names = TRUE)
  pop_fit_idx <- !grepl(pattern="complex", pop_fit_files_partial)
  pop_fit_files_partial <- pop_fit_files_partial[pop_fit_idx]
  pop_fit_files_full <- pop_fit_files_full[pop_fit_idx]
  
  # get pop files
  pop_folder <- paste0(project_dir, "/sce_calib/output/pop/")
  pop_files_partial <- list.files(path = pop_folder, full.names = FALSE)
  pop_files_full <- list.files(path = pop_folder, full.names = TRUE)
  pop_idx <- !grepl(pattern="complex", pop_files_partial)
  pop_files_partial <- pop_files_partial[pop_idx]
  pop_files_full <- pop_files_full[pop_idx]
  
  # loop through pop fit files
  pop_fit_info <- data.frame(file = rep("none", length(pop_fit_files_partial)), 
                             file_idx = rep(0, length(pop_fit_files_partial)), 
                             min_val = rep(0, length(pop_fit_files_partial)),
                             min_val_idx = rep(0, length(pop_fit_files_partial)))
  for (i in 1:length(pop_fit_files_partial)){
    
    # read in pop fit file
    pop_fit_df <- read.csv(pop_fit_files_full[i])
    
    # identify
    min_val <- min(pop_fit_df$x)
    min_val_idx <- which(pop_fit_df$x == min_val)  
    min_val_idx <- min_val_idx[length(min_val_idx)]   # note: if multiple, taking the last one
    
    # store
    pop_fit_info$file[i] <- pop_fit_files_partial[i]
    pop_fit_info$file_idx[i] <- i
    pop_fit_info$min_val[i] <- min_val
    pop_fit_info$min_val_idx[i] <- min_val_idx
    
  }
  
  # identify min of all the mins
  min_of_min <- min(pop_fit_info$min_val)
  min_of_min_idx <- which(pop_fit_info$min_val == min_of_min)
  file_idx <- pop_fit_info$file_idx[min_of_min_idx]
  file_idx <- file_idx[length(file_idx)] # if multiple, take the last one
  
  # get parameters for the best parameter
  best_param_file <- pop_files_full[file_idx]
  best_param_df <- read.csv(best_param_file)
  calib_param <- best_param_df[pop_fit_info$min_val_idx[file_idx], ]
  calib_param <- as.list(calib_param)
  
}







# Update PRMS parameters ---------------------------------------------------------------####

# read in PRMS parameter file
params <- read_prms_parms(path = prms_param_file_in)

# loop through parameters and update parameter values
for (i in 1:length(calib_param)){

  # extract param name
  this_param_name <- names(calib_param[i])

  # extract param values
  this_param_idx <- grep(this_param_name, params$parameters$name)
  this_param_val <- params$parameters$values[[this_param_idx]]

  # extract param multiplier
  param_mult <- calib_param[[this_param_name]]

  # update parameters (use multipliers to multiply existing best parameter values)
  this_param_val <- this_param_val * param_mult
  this_param_val <- round(this_param_val, digits=2)

  # store updated param values
  params$parameters$values[[this_param_idx]] <- this_param_val

}




# Export updated PRMS parameters ---------------------------------------------------------------####

# write updated PRMS param file
write_prms_parms(params, path = prms_param_file_out)



# Run PRMS ---------------------------------------------------------------####

# run PRMS
setwd(paste0(project_dir, "/gsflow"))
system2(model_run_batch_file)


