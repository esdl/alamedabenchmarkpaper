Directory: alamedabenchmarkpaper-v.0.1.0\LSTM_HybridModels_ErrorMetrics\Code
This directory contains the MATLAB scripts to run the following LSTM-based ensemble models for the Upper Alameda and Arroyo Hondo Basins

The format of the script name is LSTM_[Atmospheric forcings]_{optional model to be post-processesd}_Ensemmle[# of ensemble members]
Upper Alameda:
LSTM_Alameda_PT_Ensemble10.m: 			base case, LSTM with P&T only
LSTM_Alameda_PT_IHACRES_ALL_Ensemble10.m: 	LSTM with P&T plus all IHACRES outputs
LSTM_Alameda_PT_IHACRES_FLUXES_Ensemble10.m: 	LSTM with P&T plus IHACRES outputs related to fluxes
LSTM_Alameda_PT_IHACRES_STATES_Ensemble10.m: 	LSTM with P&T plus IHACRES outputs related to states
LSTM_Alameda_PT_IHACRES_UNC_Ensemble10.m: 	LSTM with P&T plus all outputs from the uncalibrated IHACRES 
LSTM_Alameda_PT_PRMS_ALL_Ensemble10.m:		LSTM with P&T plus all PRMS outputs
LSTM_Alameda_PT_PRMS_FLUXES_Ensemble10.m:	LSTM with P&T plus PRMS outputs related to fluxes
LSTM_Alameda_PT_PRMS_STATES_Ensemble10.m:	LSTM with P&T plus PRMS outputs related to states
LSTM_Alameda_PT_PRMS_UNC_Ensemble10.m:		LSTM with P&T plus all outputs from the uncalibrated PRMS 
LSTM_Alameda_PT_SACSMA_ALL_Ensemble10.m:	LSTM with P&T plus all SACSMA outputs
LSTM_Alameda_PT_SACSMA_FLUXES_Ensemble10.m:	LSTM with P&T plus SACSMA outputs related to fluxes
LSTM_Alameda_PT_SACSMA_STATES_Ensemble10.m:	LSTM with P&T plus SACSMA outputs related to states
LSTM_Alameda_PT_SACSMA_UNC_Ensemble10.m:	LSTM with P&T plus all outputs from the uncalibrated SACSMA 

Arroyo Hondo:
LSTM_Arroyo_PT_Ensemble10.m: 			base case, LSTM with P&T only
LSTM_Arroyo_PT_IHACRES_ALL_Ensemble10.m: 	LSTM with P&T plus all IHACRES outputs
LSTM_Arroyo_PT_IHACRES_FLUXES_Ensemble10.m: 	LSTM with P&T plus IHACRES outputs related to fluxes
LSTM_Arroyo_PT_IHACRES_STATES_Ensemble10.m: 	LSTM with P&T plus IHACRES outputs related to states
LSTM_Arroyo_PT_IHACRES_UNC_Ensemble10.m: 	LSTM with P&T plus all outputs from the uncalibrated IHACRES 
LSTM_Arroyo_PT_PRMS_ALL_Ensemble10.m:		LSTM with P&T plus all PRMS outputs
LSTM_Arroyo_PT_PRMS_FLUXES_Ensemble10.m:	LSTM with P&T plus PRMS outputs related to fluxes
LSTM_Arroyo_PT_PRMS_STATES_Ensemble10.m:	LSTM with P&T plus PRMS outputs related to states
LSTM_Arroyo_PT_PRMS_UNC_Ensemble10.m:		LSTM with P&T plus all outputs from the uncalibrated PRMS 
LSTM_Arroyo_PT_SACSMA_ALL_Ensemble10.m:	LSTM with P&T plus all SACSMA outputs
LSTM_Arroyo_PT_SACSMA_FLUXES_Ensemble10.m:	LSTM with P&T plus SACSMA outputs related to fluxes
LSTM_Arroyo_PT_SACSMA_STATES_Ensemble10.m:	LSTM with P&T plus SACSMA outputs related to states
LSTM_Arroyo_PT_SACSMA_UNC_Ensemble10.m:	LSTM with P&T plus all outputs from the uncalibrated SACSMA 

To execute these scripts, the input data file in csv format containing the P+T forcings and the output of the process-based models must be supplied.
The latest version of this file is named alameda_data_for_lstm_prms_sacsma_ihacres_20220425.csv and can be found in the directory ../Data
It is sufficient to copy this file to the current working directory toy execute the scripts.

Upon execution, the scripts will create two sub-directories, Upper_Alameda and Arroyo_Hondo.
Within them each script will have a corresponding sub-directory that follows the same naming convention of the script files.
For example, executing the script LSTM_Arroyo_PT_PRMS_ALL_Ensemble10 will create subdirectories Arroyo_Hondo and Arroyo_Hondo/LSTM_Arroyo_PT_PRMS_ALL_Ensemble10.

This directories will contain all the output cvs files needed to generate the tables and plots presented in the manuscript.

Note: the csv files contained in these ditectories directories must be copied to the working directory of the plot-generating software, maintaining the same directory structure.
For convenience, they are included in the directory ../OutputTables

Subsequently, these output tables can be processed by the plot-generating scripts described in the directory alamedabenchmarkpaper-v.0.1.0\ProcessBasedModels_SummaryTables_Plots
