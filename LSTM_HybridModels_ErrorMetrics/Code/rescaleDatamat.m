function [dMat, sMat] = rescaleDatamat(dMat, tNorm, var1, var2)
% rescales columns of dMat
% Note the two options:
%   if tNorm == 0 
%       uses standard formula x' = (x-min(x))/(max(x)-min(x))
%       saves min and max of each column in sMat
%   if tNorm == 1 (default)
%       uses standard formula x' = (x-mean(x))/std(x)
%       saves mean and std of each column in sMat
%   if tNorm == 2
%       min and max supplied
%       uses standard formula x' = (x-min(x))/(max(x)-min(x))
%       saves min and max of each column in sMat
%   if tNorm == 3
%       mean and std supplied
%       uses standard formula x' = (x-mean(x))/std(x)
%       saves mean and std of each column in sMat

% check input
if (nargin < 1 || nargin > 4)
    error('Usage: [dMat sVec] = rescaleDatamat(dMat, [tNorm, Var1, Var2])');
end
if (nargin < 4)
    var2 = [];
end
if (nargin < 3)
    var1 = [];
end
if (nargin < 2)
    tNorm = 1;
end
if (tNorm > 1 && nargin < 4)
    error('rescaleDatamat: missing user-supplied normalization parameters!');
end

% initialize
[nR, nC] = size(dMat);
sMat = zeros(2, nC);
if (tNorm == 0)
    sMat(1,:) = nanmin(dMat);
    sMat(2,:) = nanmax(dMat);
elseif (tNorm == 1)
    sMat(1,:) = nanmean(dMat);
    sMat(2,:) = nanstd(dMat);
else
    sMat(1,:) = var1;
    sMat(2,:) = var2;
end

% normalize
if (tNorm == 0)
    for n=1:nC
        if (diff(sMat(:,n)))
            dMat(:,n) = (dMat(:,n)-sMat(1,n))./(sMat(2,n)-sMat(1,n));
        else
            dMat(:,n) = zeros(nR,1);
        end
    end
elseif (tNorm == 1)
    for n=1:nC
        if (sMat(2,n))
            dMat(:,n) = (dMat(:,n)-sMat(1,n))./sMat(2,n);
        else
            dMat(:,n) = zeros(nR,1);
        end
    end
elseif (tNorm == 2)
    for n=1:nC
        if (var2-var1)
            dMat(:,n) = (dMat(:,n)-var1)./(var2-var1);
        else
            dMat(:,n) = zeros(nR,1);
        end
    end
elseif (tNorm == 3)
    for n=1:nC
        if (var2)
            dMat(:,n) = (dMat(:,n)-var1)./var2;
        else
            dMat(:,n) = zeros(nR,1);
        end
    end
end