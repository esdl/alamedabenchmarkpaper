%% Modified original code to do ensemble run
%% Variables from new data file
% This is an explanation of the columns in the csv file called alameda_data_for_lstm_prms_sacsma_ihacres_20210314.csv
% 
% Column 1 is the date
% 1) date: date 
% 
% Columns 2-5 are observed Arroyo Hondo subbasin variables
% 2) obs_precip_AH: observed precipitation (inches) in Arroyo Hondo subbasin
% 3) obs_tmaxc_AH: observed maximum temperature (degrees Celsius) in Arroyo Hondo subbasin
% 4) obs_tminc_AH: observed minimum temperature (degrees Celsius) in Arroyo Hondo subbasin
% 5) obs_flow_AH: observed streamflow (cfs) in Arroyo Hondo subbasin
% 
% Columns 6-21 are simulated Arroyo Hondo subbasin variables from an uncalibrated PRMS model
% 6) prms_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 7) prms_uncalib_sim_actet_AH: simulated actual ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 8) prms_uncalib_sim_potet_AH: simulated potential ET (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 9) prms_uncalib_sim_swrad_AH: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 10) prms_uncalib_sim_interflow_AH: simulated interflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 11) prms_uncalib_sim_sroff_AH: simulated surface runoff (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 12) prms_uncalib_sim_gwflow_AH: simulated groundwater flow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 13) prms_uncalib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
% 14) prms_uncalib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
% 15) prms_uncalib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Arroyo Hondo subbasin
% 16) prms_uncalib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 17) prms_uncalib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Arroyo Hondo subbasin
% 18) prms_uncalib_sim_hru_storage_AH: total simulated storage (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 19) prms_uncalib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Arroyo Hondo subbasin
% 20) prms_uncalib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 21) prms_uncalib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Arroyo Hondo subbasin
% 
% Columns 22-37 are simulated Arroyo Hondo subbasin variables from a calibrated PRMS model
% 22) prms_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
% 23) prms_calib_sim_actet_AH: simulated actual ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 24) prms_calib_sim_potet_AH: simulated potential ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 25) prms_calib_sim_swrad_AH: simulated shortwave radiation (Langleys) from calibrated PRMS model in Arroyo Hondo subbasin
% 26) prms_calib_sim_interflow_AH: simulated interflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
% 27) prms_calib_sim_sroff_AH: simulated surface runoff (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
% 28) prms_calib_sim_gwflow_AH: simulated groundwater flow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
% 29) prms_calib_sim_pref_flow_stor_AH: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
% 30) prms_calib_sim_slow_stor_AH: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
% 31) prms_calib_sim_soil_moist_AH: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Arroyo Hondo subbasin
% 32) prms_calib_sim_soil_moist_tot_AH: total simulated soil zone storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 33) prms_calib_sim_hru_intcpstor_AH: simulated interception storage (inches) in the canopy from calibrated PRMS model in Arroyo Hondo subbasin
% 34) prms_calib_sim_hru_storage_AH: total simulated storage (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 35) prms_calib_sim_gwres_stor_AH: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Arroyo Hondo subbasin
% 36) prms_calib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 37) prms_calib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
% 
% Columns 38-41 are observed Upper Alameda subbasin variables
% 38) obs_precip_UA: observed precipitation (inches) in Upper Alameda subbasin
% 39) obs_tmaxc_UA: observed maximum temperature (degrees Celsius) in Upper Alameda subbasin
% 40) obs_tminc_UA: observed minimum temperature (degrees Celsius) in Upper Alameda subbasin
% 41) obs_flow_UA: observed streamflow (cfs) in Upper Alameda subbasin
% 
% Columns 42-57 are simulated Upper Alameda subbasin variables from an uncalibrated PRMS model
% 42) prms_uncalib_sim_flow_UA: simulated streamflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
% 43) prms_uncalib_sim_actet_UA: simulated actual ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 44) prms_uncalib_sim_potet_UA: simulated potential ET (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 45) prms_uncalib_sim_swrad_UA: simulated shortwave radiation (Langleys) from uncalibrated PRMS model in Upper Alameda subbasin
% 46) prms_uncalib_sim_interflow_UA: simulated interflow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
% 47) prms_uncalib_sim_sroff_UA: simulated surface runoff (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
% 48) prms_uncalib_sim_gwflow_UA: simulated groundwater flow (cfs) from uncalibrated PRMS model in Upper Alameda subbasin
% 49) prms_uncalib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
% 50) prms_uncalib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
% 51) prms_uncalib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from uncalibrated PRMS model in Upper Alameda subbasin
% 52) prms_uncalib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 53) prms_uncalib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from uncalibrated PRMS model in Upper Alameda subbasin
% 54) prms_uncalib_sim_hru_storage_UA: total simulated storage (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 55) prms_uncalib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from uncalibrated PRMS model in Upper Alameda subbasin
% 56) prms_uncalib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 57) prms_uncalib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from uncalibrated PRMS model in Upper Alameda subbasin
% 
% Columns 58-73 are simulated Upper Alameda subbasin variables from a calibrated PRMS model
% 58) prms_calib_sim_flow_UA: simulated streamflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
% 59) prms_calib_sim_actet_UA: simulated actual ET (inches) from calibrated PRMS model in Upper Alameda subbasin
% 60) prms_calib_sim_potet_UA: simulated potential ET (inches) from calibrated PRMS model in Upper Alameda subbasin
% 61) prms_calib_sim_swrad_UA: simulated shortwave radiation (Langleys) from calibrated PRMS model in Upper Alameda subbasin
% 62) prms_calib_sim_interflow_UA: simulated interflow (cfs) from calibrated PRMS model in Upper Alameda subbasin
% 63) prms_calib_sim_sroff_UA: simulated surface runoff (cfs) from calibrated PRMS model in Upper Alameda subbasin
% 64) prms_calib_sim_gwflow_UA: simulated groundwater flow (cfs) from calibrated PRMS model in Upper Alameda subbasin
% 65) prms_calib_sim_pref_flow_stor_UA: simulated storage (inches) in preferential flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
% 66) prms_calib_sim_slow_stor_UA: simulated storage (inches) in gravity flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
% 67) prms_calib_sim_soil_moist_UA: simulated storage (inches) in capillary flow reservoir of soil zone from calibrated PRMS model in Upper Alameda subbasin
% 68) prms_calib_sim_soil_moist_tot_UA: total simulated soil zone storage (inches) from calibrated PRMS model in Upper Alameda subbasin
% 69) prms_calib_sim_hru_intcpstor_UA: simulated interception storage (inches) in the canopy from calibrated PRMS model in Upper Alameda subbasin
% 70) prms_calib_sim_hru_storage_UA: total simulated storage (inches) from calibrated PRMS model in Upper Alameda subbasin
% 71) prms_calib_sim_gwres_stor_UA: simulated storage (inches) in groundwater reservoir from calibrated PRMS model in Upper Alameda subbasin
% 72) prms_calib_sim_dunnian_flow_UA: simulated Dunnian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin
% 73) prms_calib_sim_hortonian_flow_UA: simulated Hortonian surface flow (inches) from calibrated PRMS model in Upper Alameda subbasin
% 
% Columns 74-95 are simulated Arroyo Hondo subbasin variables from an uncalibrated SAC-SMA model
% 74) sacsma_uncalib_sim_effrain_AH: simulated effective rainfall (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 75) sacsma_uncalib_sim_uztwc_AH: simulated upper zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 76) sacsma_uncalib_sim_uzfwc_AH: simulated upper zone free water stroage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 77) sacsma_uncalib_sim_lztwc_AH: simulated lower zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 78) sacsma_uncalib_sim_lzfsc_AH: simulated lower zone secondary free water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 79) sacsma_uncalib_sim_lzfpc_AH: simulated lower zone primary free water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 80) sacsma_uncalib_sim_adimc_AH: additional impervious area water storage content (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 81) sacsma_uncalib_sim_sett_AH: simulated cumulative total evapotranspiration (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 82) sacsma_uncalib_sim_se1_AH: simulated cumulative evapotranspiration from upper zone tension water (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 83) sacsma_uncalib_sim_se3_AH: simulated cumulative evapotranspiration from lower zone tension water (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 84) sacsma_uncalib_sim_se4_AH: simulated cumulative evapotranspiration (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 85) sacsma_uncalib_sim_se5_AH: simulated cumulative evapotranspiration from riparian zone (inches) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 86) sacsma_uncalib_sim_roimp_AH: simulated runoff from impervious area (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 87) sacsma_uncalib_sim_sdro_AH: simulated six hour sum of runoff (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 88) sacsma_uncalib_sim_ssur_AH: simulated surface runoff (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 89) sacsma_uncalib_sim_sif_AH: simulated interflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 90) sacsma_uncalib_sim_bfp_AH: simulated primary baseflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 91) sacsma_uncalib_sim_bfs_AH: simulated secondary baseflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 92) sacsma_uncalib_sim_bfcc_AH: simulated channel baseflow (bfp+bfs) (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 93) sacsma_uncalib_sim_slowflow_AH: simulated slow streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin 
% 94) sacsma_uncalib_sim_quickflow_AH: simulated quick streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 95) sacsma_uncalib_sim_flow_AH: simulated streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
% 
% Columns 96-117 are simulated Arroyo Hondo subbasin variables from a calibrated SAC-SMA model
% 96) sacsma_calib_sim_effrain_AH: simulated effective rainfall (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 97) sacsma_calib_sim_uztwc_AH: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 98) sacsma_calib_sim_uzfwc_AH: simulated upper zone free water stroage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 99) sacsma_calib_sim_lztwc_AH: simulated lower zone tension water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 100) sacsma_calib_sim_lzfsc_AH: simulated lower zone secondary free water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 101) sacsma_calib_sim_lzfpc_AH: simulated lower zone primary free water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 102) sacsma_calib_sim_adimc_AH: additional impervious area water storage content (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 103) sacsma_calib_sim_sett_AH: simulated cumulative total evapotranspiration (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 104) sacsma_calib_sim_se1_AH: simulated cumulative evapotranspiration from upper zone tension water (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 105) sacsma_calib_sim_se3_AH: simulated cumulative evapotranspiration from lower zone tension water (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 106) sacsma_calib_sim_se4_AH: simulated cumulative evapotranspiration (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 107) sacsma_calib_sim_se5_AH: simulated cumulative evapotranspiration from riparian zone (inches) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 108) sacsma_calib_sim_roimp_AH: simulated runoff from impervious area (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 109) sacsma_calib_sim_sdro_AH: simulated six hour sum of runoff (cfs) from an calibrated SAC-SMA model in Arroyo Hondo subbasin
% 110) sacsma_calib_sim_ssur_AH: simulated surface runoff (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 111) sacsma_calib_sim_sif_AH: simulated interflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 112) sacsma_calib_sim_bfp_AH: simulated primary baseflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 113) sacsma_calib_sim_bfs_AH: simulated secondary baseflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 114) sacsma_calib_sim_bfcc_AH: simulated channel baseflow (bfp+bfs) (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 115) sacsma_calib_sim_slowflow_AH: simulated slow streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 116) sacsma_calib_sim_quickflow_AH: simulated quick streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 117) sacsma_calib_sim_flow_AH: simulated streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
% 
% Columns 118-139 are simulated Upper Alameda subbasin variables from an uncalibrated SAC-SMA model
% 118) sacsma_uncalib_sim_effrain_UA: simulated effective rainfall (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 119) sacsma_uncalib_sim_uztwc_UA: simulated upper zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 120) sacsma_uncalib_sim_uzfwc_UA: simulated upper zone free water stroage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 121) sacsma_uncalib_sim_lztwc_UA: simulated lower zone tension water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 122) sacsma_uncalib_sim_lzfsc_UA: simulated lower zone secondary free water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 123) sacsma_uncalib_sim_lzfpc_UA: simulated lower zone primary free water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 124) sacsma_uncalib_sim_adimc_UA: additional impervious area water storage content (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 125) sacsma_uncalib_sim_sett_UA: simulated cumulative total evapotranspiration (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 126) sacsma_uncalib_sim_se1_UA: simulated cumulative evapotranspiration from upper zone tension water (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 127) sacsma_uncalib_sim_se3_UA: simulated cumulative evapotranspiration from lower zone tension water (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 128) sacsma_uncalib_sim_se4_UA: simulated cumulative evapotranspiration (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 129) sacsma_uncalib_sim_se5_UA: simulated cumulative evapotranspiration from riparian zone (inches) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 130) sacsma_uncalib_sim_roimp_UA: simulated runoff from impervious area (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 131) sacsma_uncalib_sim_sdro_UA: simulated six hour sum of runoff (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 132) sacsma_uncalib_sim_ssur_UA: simulated surface runoff (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 133) sacsma_uncalib_sim_sif_UA: simulated interflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 134) sacsma_uncalib_sim_bfp_UA: simulated primary baseflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 135) sacsma_uncalib_sim_bfs_UA: simulated secondary baseflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 136) sacsma_uncalib_sim_bfcc_UA: simulated channel baseflow (bfp+bfs) (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 137) sacsma_uncalib_sim_slowflow_UA: simulated slow streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 138) sacsma_uncalib_sim_quickflow_UA: simulated quick streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 139) sacsma_uncalib_sim_flow_UA: simulated streamflow (cfs) from an uncalibrated SAC-SMA model in Upper Alameda subbasin
% 
% Columns 140-161 are simulated Upper Alameda subbasin variables from a calibrated SAC-SMA model
% 140) sacsma_calib_sim_effrain_UA: simulated effective rainfall (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 141) sacsma_calib_sim_uztwc_UA: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 142) sacsma_calib_sim_uzfwc_UA: simulated upper zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 143) sacsma_calib_sim_lztwc_UA: simulated lower zone tension water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 144) sacsma_calib_sim_lzfsc_UA: simulated lower zone secondary free water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 145) sacsma_calib_sim_lzfpc_UA: simulated lower zone primary free water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 146) sacsma_calib_sim_adimc_UA: additional impervious area water storage content (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 147) sacsma_calib_sim_sett_UA: simulated cumulative total evapotranspiration (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 148) sacsma_calib_sim_se1_UA: simulated cumulative evapotranspiration from upper zone tension water (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 149) sacsma_calib_sim_se3_UA: simulated cumulative evapotranspiration from lower zone tension water (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 150) sacsma_calib_sim_se4_UA: simulated cumulative evapotranspiration (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 151) sacsma_calib_sim_se5_UA: simulated cumulative evapotranspiration from riparian zone (inches) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 152) sacsma_calib_sim_roimp_UA: simulated runoff from impervious area (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 153) sacsma_calib_sim_sdro_UA: simulated six hour sum of runoff (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 154) sacsma_calib_sim_ssur_UA: simulated surface runoff (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 155) sacsma_calib_sim_sif_UA: simulated interflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 156) sacsma_calib_sim_bfp_UA: simulated primary baseflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 157) sacsma_calib_sim_bfs_UA: simulated secondary baseflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 158) sacsma_calib_sim_bfcc_UA: simulated channel baseflow (bfp+bfs) (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 159) sacsma_calib_sim_slowflow_UA: simulated slow streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 160) sacsma_calib_sim_quickflow_UA: simulated quick streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 161) sacsma_calib_sim_flow_UA: simulated streamflow (cfs) from a calibrated SAC-SMA model in Upper Alameda subbasin
% 
% Columns 162-167 are simulated Arroyo Hondo subbasin variables from an uncalibrated IHACRES model
% 162) ihacres_uncalib_sim_effrain_AH: simulated effective rainfall (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 163) ihacres_uncalib_sim_cmd_AH: simulated catchment moisture deficit (inches) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 164) ihacres_uncalib_sim_et_AH: simulated evapotranspiration (inches) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 165) ihacres_uncalib_sim_slowflow_AH: simulated slow streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 166) ihacres_uncalib_sim_quickflow_AH: simulated quick streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 167) ihacres_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
% 
% Columns 168-173 are simulated Arroyo Hondo subbasin variables from a calibrated IHACRES model
% 168) ihacres_calib_sim_effrain_AH: simulated effective rainfall (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
% 169) ihacres_calib_sim_cmd_AH: simulated catchment moisture deficit (inches) from calibrated IHACRES model in Arroyo Hondo subbasin
% 170) ihacres_calib_sim_et_AH: simulated evapotranspiration (inches) from calibrated IHACRES model in Arroyo Hondo subbasin
% 171) ihacres_calib_sim_slowflow_AH: simulated slow streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
% 172) ihacres_calib_sim_quickflow_AH: simulated quick streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
% 173) ihacres_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
% 
% Columns 174-179 are simulated Upper Alameda subbasin variables from an uncalibrated IHACRES model
% 174) ihacres_uncalib_sim_effrain_UA: simulated effective rainfall (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
% 175) ihacres_uncalib_sim_cmd_UA: simulated catchment moisture deficit (inches) from uncalibrated IHACRES model in Upper Alameda subbasin
% 176) ihacres_uncalib_sim_et_UA: simulated evapotranspiration (inches) from uncalibrated IHACRES model in Upper Alameda subbasin
% 177) ihacres_uncalib_sim_slowflow_UA: simulated slow streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
% 178) ihacres_uncalib_sim_quickflow_UA: simulated quick streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
% 179) ihacres_uncalib_sim_flow_UA: simulated streamflow (cfs) from uncalibrated IHACRES model in Upper Alameda subbasin
% 
% Columns 180-185 are simulated Upper Alameda subbasin variables from a calibrated IHACRES model
% 180) ihacres_calib_sim_effrain_UA: simulated effective rainfall (cfs) from calibrated IHACRES model in Upper Alameda subbasin
% 181) ihacres_calib_sim_cmd_UA: simulated catchment moisture deficit (inches) from calibrated IHACRES model in Upper Alameda subbasin
% 182) ihacres_calib_sim_et_UA: simulated evapotranspiration (inches) from calibrated IHACRES model in Upper Alameda subbasin
% 183) ihacres_calib_sim_slowflow_UA: simulated slow streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin
% 184) ihacres_calib_sim_quickflow_UA: simulated quick streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin
% 185) ihacres_calib_sim_flow_UA: simulated streamflow (cfs) from calibrated IHACRES model in Upper Alameda subbasin

%% Notes
% ignore the following columns:
% 87 (sacsma_uncalib_sim_sdro_AH), 
% 109 (sacsma_calib_sim_sdro_AH), 
% 131 (sacsma_uncalib_sim_sdro_UA), 
% 153 (sacsma_calib_sim_sdro_UA)
% These columns are a SACSMA flux (six hour sum of runoff) that doesn't make sense for daily data

%% Ensemble parameters
ensembleMembers = 10; % number of models in ensemble
ensembleMedian = true; % (true: use median, false: use mean)
ensembleQuantiles = [0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95, 0.99]; % quantiles to store
plotQuantilesInd = [2, 8]; % index of two quantiles (low and high) to plot for uncertainty
numQuant = length(ensembleQuantiles);

%% File names
dataFile = 'alameda_data_for_lstm_prms_sacsma_ihacres_20220425.csv';
runName = ['LSTM_PT_PRMS_FLUXES_ENSEMBLE_', num2str(ensembleMembers)];
basinName = 'Arroyo_Hondo';
basin = 0; % 0 = arroyo, 1 = alameda
if (ispc)
    baseDir = ['.\' basinName '\' runName '\'];
else
    baseDir = ['./' basinName '/' runName '/'];
end
checkMakeDir(baseDir);

%% Start timer
disp(['Running ', runName]);
tic

%% Error metric (0=MSE, 1 = RMSE, 2 = NRMSE, 3 = NSE, 4 = KGE), averaging and quantiles
errorMetric = 3; % NSE (only used for optimization)
useMedian = true; % (true: use median, false: use mean)

%% re-run or load model
loadModel = false;
if (~loadModel)
    
    %% LSTM fixed parameters
    fixedParams = struct( ...
        'numLayers', 1, ...
        'lookBack', 270, ...
        'numHiddenUnits', 256, ...
        'DOLProb', 0.4, ...
        'MiniBatchSize', 128, ...
        'MaxEpochs', 128, ...
        'InitialLearnRate', 0.001 ...
        );

    %% setup solver environment
    nWorkers = 1;

    % gpu or cpu
    if (nWorkers > 1)
        useParallel = true;
        ExecutionEnvironment = 'cpu';
    else
        useParallel = false;
        ExecutionEnvironment = 'gpu';
    end

    lstmOptions = trainingOptions('adam', ...
        'MaxEpochs',fixedParams.MaxEpochs, ...
        'MiniBatchSize',fixedParams.MiniBatchSize, ...
        'InitialLearnRate',fixedParams.InitialLearnRate, ...
        'ExecutionEnvironment',ExecutionEnvironment, ...
        'Plots','training-progress',...
        'Verbose',1);

    %% Shuffle input sequences
    shuffleSequences = true;

    %% Test/Train set
    TrainStart = 368; % Beginning index of test set (WY 1997)
    TestStart = 6942; % Beginning index of test set (WY 2015)

    %% read data
    DataTable = readtable(dataFile); % data table
    VariableNames = DataTable.Properties.VariableNames; % variable names
    [nEntries,nVars] = size(DataTable); % number of vars
    
    %% select data and create matrices
    % variables to use in LSTM (discharge first, then rain):
    % 2) obs_precip_AH: observed precipitation (inches) in Arroyo Hondo subbasin
    % 3) obs_tmaxc_AH: observed maximum temperature (degrees Celsius) in Arroyo Hondo subbasin
    % 4) obs_tminc_AH: observed minimum temperature (degrees Celsius) in Arroyo Hondo subbasin
    % 5) obs_flow_AH: observed streamflow (cfs) in Arroyo Hondo subbasin
    
    % model outputs for LSTM
    % 22) prms_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
    % 23) prms_calib_sim_actet_AH: simulated actual ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
    % 24) prms_calib_sim_potet_AH: simulated potential ET (inches) from calibrated PRMS model in Arroyo Hondo subbasin
    % 26) prms_calib_sim_interflow_AH: simulated interflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
    % 27) prms_calib_sim_sroff_AH: simulated surface runoff (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
    % 28) prms_calib_sim_gwflow_AH: simulated groundwater flow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
    % 36) prms_calib_sim_dunnian_flow_AH: simulated Dunnian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
    % 37) prms_calib_sim_hortonian_flow_AH: simulated Hortonian surface flow (inches) from calibrated PRMS model in Arroyo Hondo subbasin
    
    % model outputs for plotting and errors
    % 6) prms_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated PRMS model in Arroyo Hondo subbasin
    % 22) prms_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated PRMS model in Arroyo Hondo subbasin
    % 95) sacsma_uncalib_sim_flow_AH: simulated streamflow (cfs) from an uncalibrated SAC-SMA model in Arroyo Hondo subbasin
    % 117) sacsma_calib_sim_flow_AH: simulated streamflow (cfs) from a calibrated SAC-SMA model in Arroyo Hondo subbasin
    % 167) ihacres_uncalib_sim_flow_AH: simulated streamflow (cfs) from uncalibrated IHACRES model in Arroyo Hondo subbasin
    % 173) ihacres_calib_sim_flow_AH: simulated streamflow (cfs) from calibrated IHACRES model in Arroyo Hondo subbasin
    
    myVars = [5,2,3,4,22:24,26:28,36:37]; % skip dates
    DataMat = table2array(DataTable(:,myVars)); % data matrix
    DateTimes = table2array(DataTable(:,1)); % dates
    Discharge = table2array(DataTable(:,5)); % discharge for plotting
    Rain = table2array(DataTable(:,2)); % rain for plotting
    PRMSuncal = table2array(DataTable(:,6)); % PRMS (uncalibrated) prediction for plotting
    PRMScalib = table2array(DataTable(:,22)); % PRMS (calibrated) prediction for plotting
    SACSMAuncal = table2array(DataTable(:,95)); % SACSMA (uncalibrated) prediction for plotting and errors
    SACSMAcalib = table2array(DataTable(:,117)); % SACSMA (calibrated) prediction for plotting and errors
    IHACRESuncal = table2array(DataTable(:,167)); % IHACRES (uncalibrated) prediction for plotting and errors
    IHACREScalib = table2array(DataTable(:,173)); % IHACRES (calibrated) prediction for plotting and errors
    [nRows,nCols] = size(DataMat); % matrix size
    numFeatures = nCols-1; % number of predictors (all those selected above)
    numResponses = 1; % number of predictands (discharge)
    
    %% plot data (DISCHARGE AND CENMET SUBSET ONLY)
    % variables to plot
    mySubPlots = myVars; % table column numbers
    nSubPlots = numel(mySubPlots);
    
    % create figure
    hFig1 = figure('units','normalized','outerposition',[0 0 1 1]);
    for n = 1:nSubPlots
        subplot(nSubPlots, 1, n) % make subplot
        %     plot(DateTimes,DataMat(:,mySubPlots(n)-1));
        plot(DateTimes,DataMat(:,n));
        %     xlabel(VariableNames(1));
        myString = VariableNames(mySubPlots(n)); % get variable name
        myLabel = split(myString, '_'); % split the name
        ylabel(myLabel);
        if (n==1) % title only on first subplot
            title([runName ' data (columns: ' num2str(mySubPlots) ')'], 'Interpreter','none');
        end
    end
    % save figure
    saveas(hFig1, [baseDir runName '_data.fig']);
    saveas(hFig1, [baseDir runName '_data.png']);
    close(hFig1);
    
    %% normalize data (subtract mean and divide by stdev)
    [RescaledMat, MeanSigma] = rescaleDatamat(DataMat);
    DischargeMean = MeanSigma(1,1); % mean
    DischargeSigma = MeanSigma(2,1); % stdev
    
    %% Train/Test indices
    begTrainInd = TrainStart; % start of training set
    endTrainInd =  TestStart - 1; % end of training set
    begTestInd = TestStart; % start of training set
    endTestInd = nRows; % end of testing set
    endInd = endTestInd; % end of data set

    nTrainSeq = endTrainInd-begTrainInd+1; % number of train sequences
    nTestSeq = endTestInd-begTestInd+1; % number of test sequences
    trainDateTimes = DateTimes(begTrainInd:endTrainInd);
    testDateTimes = DateTimes(begTestInd:endTestInd);
    PRMStrainDateTimes = trainDateTimes;
    PRMStestDateTimes = testDateTimes;
    PRMSuncaltrain = PRMSuncal(begTrainInd:endTrainInd);
    PRMSuncaltest = PRMSuncal(begTestInd:endTestInd);
    PRMScalibtrain = PRMScalib(begTrainInd:endTrainInd);
    PRMScalibtest = PRMScalib(begTestInd:endTestInd);
    SACSMAtrainDateTimes = trainDateTimes;
    SACSMAtestDateTimes = testDateTimes;
    SACSMAuncaltrain = SACSMAuncal(begTrainInd:endTrainInd);
    SACSMAuncaltest = SACSMAuncal(begTestInd:endTestInd);
    SACSMAcalibtrain = SACSMAcalib(begTrainInd:endTrainInd);
    SACSMAcalibtest = SACSMAcalib(begTestInd:endTestInd);
    IHACREStrainDateTimes = trainDateTimes;
    IHACREStestDateTimes = testDateTimes;
    IHACRESuncaltrain = IHACRESuncal(begTrainInd:endTrainInd);
    IHACRESuncaltest = IHACRESuncal(begTestInd:endTestInd);
    IHACREScalibtrain = IHACREScalib(begTrainInd:endTrainInd);
    IHACREScalibtest = IHACREScalib(begTestInd:endTestInd);
    Raintrain = Rain(begTrainInd:endTrainInd);
    Raintest = Rain(begTestInd:endTestInd);

    %% Initialize containers for ensemble results
    ensembleStatusMat = false(1,ensembleMembers);
    ensembleTrainErrMSE = NaN(1,ensembleMembers);
    ensembleTrainErrRMSE = NaN(1,ensembleMembers);
    ensembleTrainErrNRMSE = NaN(1,ensembleMembers);
    ensembleTrainErrNSE = NaN(1,ensembleMembers);
    ensembleTrainErrKGE = NaN(1,ensembleMembers);
    ensembleTestErrMSE = NaN(1,ensembleMembers);
    ensembleTestErrRMSE = NaN(1,ensembleMembers);
    ensembleTestErrNRMSE = NaN(1,ensembleMembers);
    ensembleTestErrNSE = NaN(1,ensembleMembers);
    ensembleTestErrKGE = NaN(1,ensembleMembers);
	ensembleTrainedNet = cell(1,ensembleMembers);
	ensembleTrainedInfo = cell(1,ensembleMembers);
    ensembleTrainPrediction = NaN(nTrainSeq, ensembleMembers);
    ensembleTestPrediction = NaN(nTestSeq, ensembleMembers);
    
    %% Setup train/test data for seq-1 LSTM
    
    % train cell array
    trainXCell = cell(nTrainSeq,1);
    trainYVec = zeros(nTrainSeq,1);
    
    % fill train cells
    myInd = begTrainInd;
    for n = 1:nTrainSeq
        
        % predictor
        trainXCell{n} = RescaledMat(myInd-fixedParams.lookBack+1:myInd,2:end)';
        
        % response
        trainYVec(n) = RescaledMat(myInd,1);
        
        % next index
        myInd = myInd+1;
    end
    
    if (shuffleSequences)
        % randomly order the sequences
        randOrder = randperm(nTrainSeq);
        randTrainXCell = cell(nTrainSeq,1);
        randTrainYVec = zeros(nTrainSeq,1);
        for m = 1:nTrainSeq
            % predictor
            randTrainXCell{m} = trainXCell{randOrder(m)};
            
            % response
            randTrainYVec(m) = trainYVec(randOrder(m));
        end
    end
    
    % test cell array
    testXCell = cell(nTestSeq,1);
    testYVec = zeros(nTestSeq,1);
    
    % fill test cells
    for n = 1:nTestSeq
        
        % predictor
        testXCell{n} = RescaledMat(myInd-fixedParams.lookBack+1:myInd,2:end)';
        
        % response
        testYVec(n) = RescaledMat(myInd,1);
        
        % next index
        myInd = myInd+1;
    end
    
        %% setup model environment
        if (fixedParams.numLayers) == 1
            fixedLayers = [ ...
                sequenceInputLayer(numFeatures)
                lstmLayer(fixedParams.numHiddenUnits,'OutputMode','last')
                dropoutLayer(fixedParams.DOLProb)
                fullyConnectedLayer(numResponses)
                regressionLayer];
        elseif (fixedParams.numLayers == 2)
            fixedLayers = [ ...
                sequenceInputLayer(numFeatures)
                lstmLayer(fixedParams.numHiddenUnits,'OutputMode','last')
                lstmLayer(fixedParams.numHiddenUnits,'OutputMode','last')
                dropoutLayer(fixedParams.DOLProb)
                fullyConnectedLayer(numResponses)
                regressionLayer];
        else
            error('Number of layers needs to be in range (1-2)');
        end

    %% Display and save LSTM configuration
    disp('LSTM configuration:');
    disp(['     Lookback: ', num2str(fixedParams.lookBack)]);
    disp(['     Layers: ', num2str(fixedParams.numLayers)]);
    disp(['     Hidden units: ', num2str(fixedParams.numHiddenUnits)]);
    disp(['     Dropout: ', num2str(fixedParams.DOLProb)]);
    parTable = table(fixedParams.lookBack, fixedParams.numLayers, fixedParams.numHiddenUnits, ...
        fixedParams.DOLProb, fixedParams.MaxEpochs, fixedParams.MiniBatchSize, fixedParams.InitialLearnRate, ...
        'VariableNames',{'lookBack','numLayers','numHiddenUnits','DOLProb','MaxEpochs','MiniBatchSize','InitialLearnRate'});
    writetable(parTable, [baseDir runName '_pars.csv']);

    %% load previous results to continue from last completed iteration
    if (exist([baseDir runName '.mat'], 'file'))
        load([baseDir runName '.mat']);
    end
    
   %% Loop over ensemble members
    for nEns = 1:ensembleMembers
        
        %% skip previously computed iterations
        if (ensembleStatusMat(nEns))
            continue;
        end
                        
        %% train model
        disp(['Training on all training samples (ensemble member ', num2str(nEns), ') ...']);
        tic
        if (shuffleSequences)
            [trainedNet, trainedInfo] = trainNetwork(randTrainXCell,randTrainYVec,fixedLayers,lstmOptions);
        else
            [trainedNet, trainedInfo] = trainNetwork(trainXCell,trainYVec,fixedLayers,lstmOptions);
        end
        disp('Finished training network ...')
        toc

        % store model
        ensembleTrainedNet{nEns} = trainedNet;
        ensembleTrainedInfo{nEns} = trainedInfo;
        
        % close training window
        delete(findall(0));
        
        % save
        disp('Saving results ...')
        save([baseDir runName '.mat'], '-v7.3');
        
        %% predict all training values
        disp('Predicting all training samples ...')
        tic
        predTrainVals = predict(trainedNet,trainXCell,'MiniBatchSize',1);
        disp('Finished predicting training samples ...')
        toc
        
        % undo rescaling
        predTrainVals = predTrainVals .* DischargeSigma + DischargeMean;
        trainY = trainYVec .* DischargeSigma + DischargeMean;
        
        % get median prediction if ensemble, just copy if not
        trainedY = predTrainVals;
        
        % eliminate negative discharge
        trainedY(trainedY < 0) = realmin;
        
        % compute error (model vs. observation)
        [myMSE, myRMSE, myNRMSE] = MSE(trainedY, trainY);
        myNSE = NSE(trainedY, trainY);
        myKGE = KGE(trainedY, trainY);
        disp(['Training error: NSE = ', num2str(myNSE), ', MSE = ', num2str(myMSE), ...
            ', RMSE = ', num2str(myRMSE), ', NRMSE = ', num2str(myNRMSE), ', KGE = ', num2str(myKGE)]);
        ensembleTrainErrMSE(nEns) = myMSE;
        ensembleTrainErrRMSE(nEns) = myRMSE;
        ensembleTrainErrNRMSE(nEns) = myNRMSE;
        ensembleTrainErrNSE(nEns) = myNSE;
        ensembleTrainErrKGE(nEns) = myKGE;
        
        % store training prediction in ensemble
        ensembleTrainPrediction(:,nEns) = trainedY;
        
        % save
        disp('Saving results ...')
        save([baseDir runName '.mat'], '-v7.3');
        
        %% predict all test values
        disp('Predicting test samples with trained network ...')
        tic
        predTestVals = predict(trainedNet,testXCell,'MiniBatchSize',1);
        disp('Finished predicting test samples ...')
        toc
        
        % undo rescaling
        predTestVals = predTestVals.*MeanSigma(2,1)+MeanSigma(1,1);
        testY = testYVec*MeanSigma(2,1)+MeanSigma(1,1);
        
        % get median prediction
        predictedY = predTestVals;
        
        % eliminate negative discharge
        predictedY(predictedY < 0) = realmin;
        
        % compute error (model vs. observation)
        [myMSE, myRMSE, myNRMSE] = MSE(predictedY, testY);
        myNSE = NSE(predictedY, testY);
        myKGE = KGE(predictedY, testY);
        ensembleTestErrMSE(nEns) = myMSE;
        ensembleTestErrRMSE(nEns) = myRMSE;
        ensembleTestErrNRMSE(nEns) = myNRMSE;
        ensembleTestErrNSE(nEns) = myNSE;
        ensembleTestErrKGE(nEns) = myKGE;

        % store training prediction in ensemble
        ensembleTestPrediction(:,nEns) = predictedY;

        % display error
        disp(['Testing error: NSE = ', num2str(myNSE), ', MSE = ', num2str(myMSE), ', RMSE = ', num2str(myRMSE), ', NRMSE = ', num2str(myNRMSE), ', KGE = ', num2str(myKGE)]);
        
        % save
        disp('Finished testing network ...')
        toc
        
        % set status
        ensembleStatusMat(nEns) = true;
        
        % save
        disp('Saving results ...')
        save([baseDir runName '.mat'], '-v7.3');
        
    end % end ensemble loop
    
end % end re-run or load model

%% load model and variables
load([baseDir runName '.mat']);

%% make and save quantile tables
ensembleTrainTable = table(trainDateTimes, ensembleTrainPrediction,'VariableNames',{'DateTime','Run'});
ensembleTestTable = table(testDateTimes, ensembleTestPrediction,'VariableNames',{'DateTime','Run'});
writetable(ensembleTrainTable, [baseDir runName '_ensembleTrainTable.csv']);
writetable(ensembleTestTable, [baseDir runName '_ensembleTestTable.csv']);

% Generate ensemble prediction and quantiles

% predictions
if (useMedian)
    ensembleTrainedY = nanmedian(ensembleTrainPrediction, 2);
    ensemblePredictedY = nanmedian(ensembleTestPrediction, 2);
else
    ensembleTrainedY = nanmean(ensembleTrainPrediction, 2);
    ensemblePredictedY = nanmean(ensembleTestPrediction, 2);
end

% quantiles
ensembleTrainQuantiles = quantile(ensembleTrainPrediction, ensembleQuantiles, 2);
ensembleTestQuantiles = quantile(ensembleTestPrediction, ensembleQuantiles, 2);
ensembleLowTrainQuantile = ensembleTrainQuantiles(:, plotQuantilesInd(1));
ensembleHighTrainQuantile = ensembleTrainQuantiles(:, plotQuantilesInd(2));
ensembleLowTestQuantile = ensembleTestQuantiles(:, plotQuantilesInd(1));
ensembleHighTestQuantile = ensembleTestQuantiles(:, plotQuantilesInd(2));

% save quantiles
trDT = table(trainDateTimes,'VariableNames',{'DateTime'});
teDT = table(testDateTimes,'VariableNames',{'DateTime'});
trQT = array2table(ensembleTrainQuantiles);
teQT = array2table(ensembleTestQuantiles);
for i = 1:numQuant
    trQT.Properties.VariableNames(i) = {['Quantile_',num2str(ensembleQuantiles(i)*100,'%d')]};
    teQT.Properties.VariableNames(i) = {['Quantile_',num2str(ensembleQuantiles(i)*100,'%d')]};
end
quantTrainTable = [trDT,trQT];
quantTestTable = [teDT,teQT];
writetable(quantTrainTable, [baseDir runName '_trainQuant.csv']);
writetable(quantTestTable, [baseDir runName '_testQuant.csv']);

% save
disp('Saving results ...')
save([baseDir runName '.mat'], '-v7.3');

%% compute error

% make tables
trainTable = table(trainDateTimes, trainY, ensembleTrainedY, PRMSuncaltrain, PRMScalibtrain, SACSMAuncaltrain, SACSMAcalibtrain, IHACRESuncaltrain, IHACREScalibtrain, Raintrain, ...
    'VariableNames',{'DateTime','Observed',runName,'PRMS_uncal','PRMS_calib','SACSMA_uncal','SACSMA_calib','IHACRES_uncal','IHACRES_calib','Obs_Precip'});
testTable = table(testDateTimes, testY, ensemblePredictedY, PRMSuncaltest, PRMScalibtest, SACSMAuncaltest, SACSMAcalibtest, IHACRESuncaltest, IHACREScalibtest, Raintest, ...
    'VariableNames',{'DateTime','Observed',runName,'PRMS_uncal','PRMS_calib','SACSMA_uncal','SACSMA_calib','IHACRES_uncal','IHACRES_calib','Obs_Precip'});
[errTable, yearlyTable, monthlyTable] = errorMetrics(trainTable, testTable, basin, runName);
errMat = table2array(errTable);

% save trained and predicted
writetable(errTable, [baseDir runName '_err.csv']);
writetable(yearlyTable, [baseDir runName '_yearly.csv']);
writetable(monthlyTable, [baseDir runName '_monthly.csv']);
writetable(trainTable, [baseDir runName '_train.csv']);
writetable(testTable, [baseDir runName '_test.csv']);

%% plot results (old plot)
% NOTE: add shaded quantile area

% colors that are robust to colorblind, b&w printing, etc.
Color0 = [0,0,0];
Color1 = [230,97,1]./255;
Color2 = [253,184,99]./255; 
Color3 = [178,171,210]./255; 
Color4 = [94,60,153]./255;
Color5 = [166,206,227]./255;
Color6 = [31,120,180]./255;

hFig2 = figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf, 'defaultAxesColorOrder', [Color0; Color0; get(gcf, 'defaultAxesColorOrder')]);
hold on;
yyaxis left;
plot(DateTimes, Rain, '-', 'color', rgb('lightslategray'), 'LineWidth', 1);
set(gca,'ydir','reverse');
ylim([0 10]);
axL = gca;
axL.FontName = 'Arial';
axL.FontSize = 14;
yyaxis right;
p1 = plot(DateTimes, Discharge, '-', 'color', Color0, 'LineWidth', 7);
p2 = plot(DateTimes, SACSMAuncal, '-', 'color', Color1, 'LineWidth', 3);
p3 = plot(DateTimes, SACSMAcalib, '-', 'color', Color2, 'LineWidth', 3);
p4 = plot(DateTimes, IHACRESuncal, '-', 'color', Color3, 'LineWidth', 3);
p5 = plot(DateTimes, IHACREScalib, '-', 'color', rgb('green'), 'LineWidth', 3);
p6 = plot(DateTimes, PRMSuncal, '-', 'color', Color4, 'LineWidth', 3);
p7 = plot(DateTimes, PRMScalib, '-', 'color', Color5, 'LineWidth', 3);
p8 = plot(trainDateTimes, ensembleTrainedY, '-', 'color', Color6, 'LineWidth', 3);
p9 = plot(testDateTimes, ensemblePredictedY, '-', 'color', rgb('deepskyblue'),  'LineWidth', 3);
% p2.Color(4) = 0.8;
% p3.Color(4) = 0.8;
% p4.Color(4) = 0.8;
lH = legend({'Daily Precip', 'Observed Discharge', 'SACSMA (uncalibrated)', 'SACSMA (calibrated)', 'IHACRES (uncalibrated)', 'IHACRES (calibrated)', 'PRMS (uncalibrated)', 'PRMS (calibrated)', 'LSTM (train)', 'LSTM (test)'}, ...
    'Location', 'NorthEast', 'FontName', 'Arial', ...
    'FontSize', 14, 'color', rgb('GhostWhite'));
axR = gca;
axR.FontName = 'Arial';
axR.FontSize = 14;
xlim([DateTimes(1) DateTimes(end)]);
ylim([0,4000]);
set(gca, 'color', rgb('extralightgray'));
hold off
title([runName ' Discharge NSE (train/test): ' ...
    num2str(errMat(1,8)) '/' num2str(errMat(1,9)) ' (LSTM), ' ...
    num2str(errMat(2,8)) '/' num2str(errMat(2,9)) ' (PRMSunc), ' ...
    num2str(errMat(3,8)) '/' num2str(errMat(3,9)) ' (PRMScal)'], 'interpreter', 'none');
yyaxis left;
ylabel('Daily Rainfall', 'FontSize', 16, 'FontName', 'Arial');
yyaxis right;
ylabel({'Discharge'}, 'FontSize', 16, 'FontName', 'Arial');

% save figure
saveas(hFig2, [baseDir runName '.fig']);
saveas(hFig2, [baseDir runName '.png']);
close(hFig2);

%% plot results (old plot)
% NOTE: add shaded quantile area

% colors that are robust to colorblind, b&w printing, etc.
Color0 = [0,0,0];
Color1 = [230,97,1]./255;
Color2 = [253,184,99]./255; 
Color3 = [178,171,210]./255; 
Color4 = [94,60,153]./255;
Color5 = [166,206,227]./255;
Color6 = [31,120,180]./255;

hFig3 = figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf, 'defaultAxesColorOrder', [Color0; Color0; get(gcf, 'defaultAxesColorOrder')]);
hold on;
yyaxis left;
p1 = plot(DateTimes, Rain, '-', 'color', rgb('lightslategray'), 'LineWidth', 1);
set(gca,'ydir','reverse');
ylim([0 10]);
axL = gca;
axL.FontName = 'Arial';
axL.FontSize = 14;
yyaxis right;
p2 = plot(DateTimes, Discharge, '-', 'color', Color0, 'LineWidth', 6);
p3 = plot(trainDateTimes, ensembleHighTrainQuantile, '-', 'color', rgb('lightgray'),  'LineWidth', 2);
p4 = plot(trainDateTimes, ensembleTrainedY, '-', 'color', Color6, 'LineWidth', 3);
p5 = plot(trainDateTimes, ensembleLowTrainQuantile, '-', 'color', rgb('darkgray'),  'LineWidth', 2);
p6 = plot(testDateTimes, ensembleHighTestQuantile, '-', 'color', rgb('lightgray'),  'LineWidth', 2);
p7 = plot(testDateTimes, ensemblePredictedY, '-', 'color', rgb('deepskyblue'),  'LineWidth', 4);
p8 = plot(testDateTimes, ensembleLowTestQuantile, '-', 'color', rgb('darkgray'),  'LineWidth', 2);
l = legend({'Daily Precip', 'Observed Discharge', 'LSTM 95% (train)', 'LSTM (train)', 'LSTM 5% (train)', 'LSTM 95% (test)', 'LSTM (test)', 'LSTM 5% (test)'}, ...
    'Location', 'NorthEast', 'FontName', 'Arial', ...
    'FontSize', 14, 'color', rgb('GhostWhite'));
axR = gca;
axR.FontName = 'Arial';
axR.FontSize = 14;
xlim([DateTimes(1) DateTimes(end)]);
ylim([0,4000]);
set(gca, 'color', rgb('extralightgray'));
hold off
title([runName ' Discharge NSE (train/test): ' ...
    num2str(errMat(1,8)) '/' num2str(errMat(1,9)) ' (LSTM), ' ...
    num2str(errMat(2,8)) '/' num2str(errMat(2,9)) ' (PRMSunc), ' ...
    num2str(errMat(3,8)) '/' num2str(errMat(3,9)) ' (PRMScal)'], 'interpreter', 'none');
yyaxis left;
ylabel('Daily Rainfall', 'FontSize', 16, 'FontName', 'Arial');
yyaxis right;
ylabel({'Discharge'}, 'FontSize', 16, 'FontName', 'Arial');

% save figure
saveas(hFig3, [baseDir runName '_ensemble.fig']);
saveas(hFig3, [baseDir runName '_ensemble.png']);
close(hFig3);

%% save all
disp('Saving results ...')
save([baseDir runName '.mat'], '-v7.3');

%% Start timer
disp(['Finished running ', runName]);
toc
