Directory: alamedabenchmarkpaper-v.0.1.0/LSTM_HybridModels_ErrorMetrics/Data
This directory contains the input data file for the LSTM-based models.
The version currently available is alameda_data_for_lstm_prms_sacsma_ihacres_20220425.csv and it contains the precipitation and temperature forcings as well as the variables output by the process-based models
