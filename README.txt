Directory: alamedabenchmarkpaper-v.0.1.0
This directory contains two subdirectories (LSTM_HybridModels_ErrorMetrics and ProcessBasedModels_SummaryTables_Plots) containing the code and data to run the LSTM-based ensemble models for the Upper Alameda and Arroyo Hondo Basins, as well as the code and data needed to generate the plots presented in the manuscript
Please refer to the individual README.txt files included in these directories and their sub-directories for detailed instructions
