This repository contains data and scripts for the Alameda streamflow prediction paper titled:
"Streamflow prediction at the inte rsection of physics and machine learning: a case study of two Mediterranean-climate watersheds"

The repository can be cited as:
Adera, Saalem, & Bellugi, Dino. (2024). Data and scripts for Alameda streamflow prediction paper (0.2.0) [Data set]. Zenodo.  https://doi.org/10.5281/zenodo.11044019.


